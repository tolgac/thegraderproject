﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System;
using UIKit;
using TheGrader;
using TheGrader.iOS;

[assembly: ExportRenderer(typeof(GestureView), typeof(GestureLabelRenderer))]

namespace TheGrader.iOS
{
	public class GestureLabelRenderer:ViewRenderer<GestureView, UIView>
	{
		UIView _view;
		UITapGestureRecognizer tapGestureRecognizer;
		UILongPressGestureRecognizer longPressGestureRecognizer;
		//UIPinchGestureRecognizer pinchGestureRecognizer;
		//UIPanGestureRecognizer panGestureRecognizer;
		//UISwipeGestureRecognizer swipeGestureRecognizer;
		//UIRotationGestureRecognizer rotationGestureRecognizer;


		protected override void OnElementChanged(ElementChangedEventArgs<GestureView> e)
		{
			base.OnElementChanged(e);

			longPressGestureRecognizer = new UILongPressGestureRecognizer(() =>
			{

				if (longPressGestureRecognizer.State == UIGestureRecognizerState.Ended)
				{
					e.NewElement.OnLongPressed(e.NewElement, new EventArgs());
				}

			});
			tapGestureRecognizer = new UITapGestureRecognizer(() => { 
				if (tapGestureRecognizer.State == UIGestureRecognizerState.Ended)
				{
					 e.NewElement.onSingleTapped(e.NewElement,new EventArgs());
				}
			});

			if (Control == null)
			{
				// Instantiate the native control and assign it to the Control property
				_view = new UIView();
				SetNativeControl(_view);

			}
			if (e.OldElement != null)
			{
				if (longPressGestureRecognizer != null)
				{
					this.RemoveGestureRecognizer(longPressGestureRecognizer);
				}

				if (tapGestureRecognizer !=null)
				{
					this.RemoveGestureRecognizer(tapGestureRecognizer);
				}
			}
			if (e.NewElement != null)
			{
				this.AddGestureRecognizer(longPressGestureRecognizer);
				this.AddGestureRecognizer(tapGestureRecognizer);

			}
		}

	}
}

