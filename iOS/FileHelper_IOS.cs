﻿using System;
using System.IO;
using TheGrader.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(FileHelper_IOS))]
namespace TheGrader.iOS
{
    public class FileHelper_IOS : IFileHelper
    {
        public bool FileExist(string path)
        {
            return File.Exists(path);
           // Environment.SpecialFolder.ApplicationData
        }

        public string GetApplicationDataFolder()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).Replace(".config", string.Empty);
        }

        public string GetCurrentDirectory()
        {
            return Environment.CurrentDirectory;
        }

        public string MakeDir(string parentfolder, string foldername)
        {
            var fullpath = Path.Combine(parentfolder, foldername);
            if (!Directory.Exists(fullpath)) {
                var di = Directory.CreateDirectory(fullpath);
                return di.FullName;
            }
            return fullpath;


        }
        public string Combine(string path1, string path2)
        {
            return Path.Combine(path1, path2);
        }
    }
}
