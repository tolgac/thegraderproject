﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using SVG.Forms.Plugin.iOS;
using System.Reflection;

namespace TheGrader.iOS
{
	[Register( "AppDelegate" )]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching( UIApplication app, NSDictionary options )
		{
			global::Xamarin.Forms.Forms.Init();
			SvgImageRenderer.Init();

			LoadApplication( new TheGraderApp(new IOSSetup()) );

			return base.FinishedLaunching( app, options );
		}
	}
}
