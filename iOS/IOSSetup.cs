using Autofac;
using SQLite.Net;
using SQLite.Net.Platform.XamarinIOS;
using System.IO;
using System;
using ImageCircle.Forms.Plugin.iOS;

namespace TheGrader.iOS
{
	public class IOSSetup : AppSetup
	{
		/// <summary>
		/// Platforma özel sqlite connection döndürür.
		/// </summary>
		/// <returns>The connection.</returns>
		public SQLiteConnectionWithLock GetConnection()
		{
			var dbFilname = "TheGraderDB.db3";
			 
			string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal); // Documents folder
			string libraryPath = Path.Combine(documentsPath, "..", "Library");

			var path = Path.Combine(libraryPath, dbFilname);
			if (File.Exists(path))
			{
				//System.IO.File.Delete(path);

			}
			var cons = new SQLiteConnectionString(path, false);
			var plat = new SQLitePlatformIOS();
			var conn = new SQLiteConnectionWithLock(plat, cons);
			return conn;
		}
		
		protected override void RegisterDependencies(ContainerBuilder cb)
		{
            // Asagidaki kodu, test amacli senkron db ihtiyacina karsilik tutabiliriz
			//string folder = Environment.GetFolderPath( Environment.SpecialFolder.Personal );
			//var conn = new SQLite.Net.SQLiteConnection( new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS(),
			//                                            System.IO.Path.Combine( folder, "stocks.db" ) );
			//conn.CreateTable<TheGraderCore.Class>();
			//conn.CreateTable<TheGraderCore.PlusMinus>();
			//conn.CreateTable<TheGraderCore.Student>();
			//conn.CreateTable<TheGraderCore.Class_Student>();
			//conn.Close();

			var asyncDb = new SQLite.Net.Async.SQLiteAsyncConnection(() => GetConnection());

            asyncDb.CreateTableAsync<TheGraderCore.PlusMinus>().ContinueWith( t => { System.Diagnostics.Debug.WriteLine( "PlusMinus Table created!" ); } );
            asyncDb.CreateTableAsync<TheGraderCore.Class>().ContinueWith( t => { System.Diagnostics.Debug.WriteLine( "Class Table created!" ); } );
            asyncDb.CreateTableAsync<TheGraderCore.Student>().ContinueWith( t => { System.Diagnostics.Debug.WriteLine( "Student Table created!" ); } );
            asyncDb.CreateTableAsync<TheGraderCore.Class_Student>().ContinueWith( t => { System.Diagnostics.Debug.WriteLine( "Class_Student Table created!" ); } );
            asyncDb.CreateTableAsync<TheGraderCore.Session>().ContinueWith(t => { System.Diagnostics.Debug.WriteLine("Session Table created!"); });

			//Register Persistance
			cb.RegisterGeneric(typeof(DBCore.Repository<>)).As(typeof(DBCore.IRepository<>)).WithParameter("db", asyncDb).InstancePerLifetimeScope();

			base.RegisterDependencies(cb); 
            ImageCircleRenderer.Init();
		}
	}
}

