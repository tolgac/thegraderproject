﻿using System;
using SQLite.Net.Attributes;
namespace TheGraderCore
{
    public class Session
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public int SchoolID { get; set; }
        public int ClassID { get; set; }
        public int DayOfWeek { get; set; }
        public long StartTime { get; set; }//10:30
        public long EndTime { get; set; }//11:30
        public int LessonOrder { get; set; }//1.ders 3.des vs.
    }
}
