﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TheGraderCore
{
	public interface ISchoolService
	{
		 List<School> GetSchoolList();
		Task<int> AddSchool(School School);
		Task DeleteSchool(School School);
		Task EditSchool(School School);
		string GetSchoolNameByID(int id);
		Task<School> GetSchoolByID(int id);
	}
}

