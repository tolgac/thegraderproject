﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace TheGraderCore
{
	public interface IClassStudentService
	{
	 	List<Class_Student> GetStudentClass();
		List<Class_Student> GetStudentClass( System.Linq.Expressions.Expression<Func<Class_Student, bool>> expression );

		Task<int> AddStudent(Class_Student aClassSutdent);
		Task<int> DeleteClassStudent(Class_Student aClassSutdent);
		Task<List<Class_Student>> GetStudentsByClassID(int ID);
	}
}
