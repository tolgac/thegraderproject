﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TheGraderCore
{
	public interface IClassService
	{
		//DTO kullanılabilir

		Task<Class> GetClassByID(int ID);
		List<Class> GetClassList();
		Task<int> AddClass(TheGraderCore.Class Class);
		Task DeleteClass(TheGraderCore.Class Class);
		//Task<int> AddModelClass(TheGraderCore.NewClassModel Class);
		Task<List<Class>> GetClassesBySchoolID(int SchoolID);
		Task EditClass(TheGraderCore.Class Class);
	}
}

