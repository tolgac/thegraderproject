﻿using System;
using System.IO;
using TheGrader.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(FileHelper_Droid))]
namespace TheGrader.Droid
{
    public class FileHelper_Droid:IFileHelper
    {
        public bool FileExist(string path)
        {
            return File.Exists(path);
        }
       
        public string GetApplicationDataFolder()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        }

        public string GetCurrentDirectory()
        {
            return Environment.CurrentDirectory;
        }

        public string MakeDir(string parentfolder, string foldername)
        {
            var fullpath = Path.Combine(parentfolder, foldername);
            if (!Directory.Exists(fullpath)) {
                var di = Directory.CreateDirectory(fullpath);
                return di.FullName;
            }
            return fullpath;
        }

        public string Combine(string path1, string path2)
        {
            return Path.Combine(path1, path2);
        }
    }
}
