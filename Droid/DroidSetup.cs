﻿using Autofac;
using SQLite.Net;
using SQLite.Net.Platform.XamarinAndroid;
using System.IO;
using ImageCircle.Forms.Plugin.Droid;

namespace TheGrader.Droid
{
	public class DroidSetup : AppSetup
	{
		/// <summary>
		/// Platforma özel sqlite connection döndürür.
		/// </summary>
		/// <returns>The connection.</returns>
		public SQLiteConnectionWithLock GetConnection()
		{

			var dbFilname = "TheGraderDB.db3";

			string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal); // Documents folder
			//

			var path = Path.Combine(documentsPath, dbFilname);
			if (File.Exists(path))
			{
				//System.IO.File.Delete(path);

			}
			var cons = new SQLiteConnectionString(path, false);
			var plat = new  SQLitePlatformAndroid();
			var conn = new SQLiteConnectionWithLock(plat, cons);
			return conn;
		}

		protected override void RegisterDependencies(ContainerBuilder cb)
		{
			var asyncDb = new SQLite.Net.Async.SQLiteAsyncConnection(() => GetConnection());

         
            asyncDb.CreateTableAsync<TheGraderCore.PlusMinus>().ContinueWith(t => { System.Diagnostics.Debug.WriteLine("PlusMinus Table created!"); });
            asyncDb.CreateTableAsync<TheGraderCore.Class>().ContinueWith(t => { System.Diagnostics.Debug.WriteLine("Class Table created!"); });
            asyncDb.CreateTableAsync<TheGraderCore.Student>().ContinueWith(t => { System.Diagnostics.Debug.WriteLine("Student Table created!"); });
            asyncDb.CreateTableAsync<TheGraderCore.Class_Student>().ContinueWith(t => { System.Diagnostics.Debug.WriteLine("Class_Student Table created!"); });
            asyncDb.CreateTableAsync<TheGraderCore.Session>().ContinueWith(t => { System.Diagnostics.Debug.WriteLine("Session Table created!"); });


			//Register Persistance
			cb.RegisterGeneric(typeof(DBCore.Repository<>)).As(typeof(DBCore.IRepository<>)).WithParameter("db", asyncDb).InstancePerLifetimeScope();


			base.RegisterDependencies(cb);
            ImageCircleRenderer.Init();
		}
	}
}

