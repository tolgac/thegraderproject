﻿using System.Collections.Generic;
using TheGraderCore;
using System.Threading.Tasks;

namespace DBCore
{
	public class SchoolService:ISchoolService
	{
		#region Ctor
		public SchoolService(IRepository<School> SchoolRepo)
		{
			this.SchoolRepo = SchoolRepo;
		}
		#endregion

		#region Fields
		private readonly IRepository<School> SchoolRepo;
		#endregion

		#region Public Methods
		public  List<School> GetSchoolList()
		{
			var result = Task.Run(() => SchoolRepo.Get());
			return result.Result;
		}
		public async Task<int> AddSchool(School school)
		{
			return await SchoolRepo.Insert(school);
		}
		public async Task DeleteSchool(School school)
		{
			//TODO: Cascade must be handled
			await SchoolRepo.Delete(school);

		}

		public async Task EditSchool(School School)
		{
			await SchoolRepo.Update(School);
		}

		public  string	 GetSchoolNameByID(int id)
		{
			var school = Task.Run(() => SchoolRepo.Get(id));
			if (school.Result==null)
			{
				return string.Empty;
			}
			return school.Result.Name;
		}

		public async Task<School> GetSchoolByID(int id)
		{
			var school = await SchoolRepo.Get(id);
			return school;
		}
		#endregion
	}
}

