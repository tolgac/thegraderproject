﻿using System.Collections.Generic;
using TheGraderCore;
using System;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Linq.Expressions;

namespace DBCore
{
	public class PlusMinusService : IPlusMinusService
	{
		private readonly IRepository<TheGraderCore.PlusMinus> _PlusMinusRepo;

		public PlusMinusService( IRepository<TheGraderCore.PlusMinus> aRepo )
		{
			_PlusMinusRepo = aRepo;
		}

		public async Task<int> NewPlusMinus( PlusMinus aPlusMinus )
		{
			return await _PlusMinusRepo.Insert( aPlusMinus );
		}

		//private Expression< Func<TheGraderCore.PlusMinus, bool> > ClassStudentFilter( int classID, int studentID )
		//{
		//	return x => x.ClassID == classID && x.StudentID == studentID;
		//}

		public int GetPlusCount( int classID, int studentID )
		{
			var result = Task.Run( () => _PlusMinusRepo.GetList<PlusMinus>( x => x.ClassID == classID && x.StudentID == studentID && x.IsPlus == true ) );


			// x.ClassID == classID /*&& x.StudentID == studentID && x.IsPlus == true*/

			return result.Result.Count;
		}

		public int GetMinusCount( int classID, int studentID )
		{
			var result = Task.Run( () => _PlusMinusRepo.GetList<PlusMinus>( x => x.ClassID == classID && x.StudentID == studentID && x.IsPlus == false )  );
			return result.Result.Count;
		}

		public List<PlusMinus> GetStudentPlusMinusList( int classID, int studentID )
		{
			return null;
		}

		public List<PlusMinus> GetClassPlusMinusList( int studentID )
		{
			return null;
		}

		public List<PlusMinus> GetPlusMinusList()
		{
			var result = Task.Run( () => _PlusMinusRepo.Get() );
			return result.Result;
		}

	}
}
