﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TheGraderCore;

namespace DBCore
{
	public class ClassStudentService : IClassStudentService
	{
		private readonly IRepository<Class_Student> _ClassStudentRepo;

		public ClassStudentService(IRepository<Class_Student> Repo)
		{
			this._ClassStudentRepo = Repo;
		}

		public async Task<int> AddStudent(Class_Student StudentClass)
		{
			return await _ClassStudentRepo.Insert(StudentClass);
		}

		public Task<int> DeleteClassStudent(Class_Student aClassSutdent)
		{
            return _ClassStudentRepo.Delete( aClassSutdent );
		}

		public List<Class_Student> GetStudentClass()
		{
			var result = Task.Run( () => _ClassStudentRepo.Get() );
			return result.Result;
		}

		public List<Class_Student> GetStudentClass(Expression<Func<Class_Student, bool>> expression)
		{
			var result = Task.Run( () => _ClassStudentRepo.GetList<Class_Student>( expression ) );
			return result.Result;
		}

        public Task< List<Class_Student> > GetStudentsByClassID(int classID)
		{
            return Task.Run( () => _ClassStudentRepo.GetList<Class_Student>(x => x.ClassID == classID) );

		}
	}
}
