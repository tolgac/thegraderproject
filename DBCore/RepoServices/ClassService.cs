﻿using System.Collections.Generic;
using TheGraderCore;
using System.Threading.Tasks;

namespace DBCore
{
	public class ClassService : TheGraderCore.IClassService
	{
		private readonly IRepository<TheGraderCore.Class> ClassRepo;
		private readonly IRepository<TheGraderCore.Student> StudentRepo;

		public ClassService(IRepository<TheGraderCore.Class> classRepo, IRepository<TheGraderCore.Student> studentRepo)
		{
			
			this.ClassRepo = classRepo;
			this.StudentRepo = studentRepo;
		}

		public Task<Class> GetClassByID(int ID)
		{
			var result = Task.Run(() => ClassRepo.Get(ID));
			return result;
		}

		public   List<Class> GetClassList()
		{
			var result =Task.Run( () => ClassRepo.Get()); 
			return  result.Result;
		}

		public async  Task<int> AddClass(TheGraderCore.Class pClass)
		{

			//Class.Students.Add(1);

			return await ClassRepo.Insert(pClass);
			// var result = Task.Run(() => ClassRepo.Insert(Class));
			//return result.Result;
			 
		}

		public async Task DeleteClass(TheGraderCore.Class pClass)
		{
			await ClassRepo.Delete(pClass);
		}

		public async Task EditClass(TheGraderCore.Class pClass)
		{
			await ClassRepo.Update(pClass);
		}

		//public async Task<int> AddModelClass(Models.NewClassModel Class)
		//{
		//	var domainclass = new TheGraderCore.Class();
		//	domainclass.Name = Class.ClassName;
		//	domainclass.SchoolID = Class.SchoolID;

		//	return await ClassRepo.Insert(domainclass);;
		//}

		public async Task<List<Class>> GetClassesBySchoolID(int SchoolID)
		{
			return await ClassRepo.GetList<Class>(x => x.SchoolID == SchoolID,null);

		}

	}
}

