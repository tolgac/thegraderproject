﻿// Created by oaksu
// Project: The Grader
//

using System;
using System.Collections.Generic;
using SQLite.Net.Async;
using SQLite.Net;
using System.Threading.Tasks;
using System.Linq.Expressions;
using SQLiteNetExtensionsAsync.Extensions;
using System.Collections;

namespace DBCore
{
	public class Repository<T> : IRepository<T> where T :class ,new()
	{
        #region Fields
        private SQLiteAsyncConnection db;
        #endregion
		 
		private async  Task<SQLite.Net.CreateTablesResult> CreateTable()
		{
			//string cmdText = string.Format("SELECT name FROM sqlite_master WHERE type='table' AND name='{0}'", typeof(T).Name);

			//var r = await this.db.ExecuteScalarAsync<string>(cmdText);
			//if (r==null)
			//{
			var r = await this.db.CreateTableAsync<T>();
			return r;

			//}
			          
		}

		#region Ctor 
		public Repository(SQLiteAsyncConnection db)
		{
			this.db = db;
			//this.db.CreateTableAsync<T>();
		 
		}
		#endregion

		#region IRepository implementation
		public async Task<List<T>> Get()
		{
			 
			 return await db.Table<T>().ToListAsync();
		}

		public async Task<T> Get(int id)
		{
			return await db.FindAsync<T>(id);
		}

		public async Task< List<T> > GetList<TValue>( Expression<Func<T, bool>> predicate = null, 
		                                              Expression<Func<T, TValue>> orderby = null )
		{
			var q = db.Table<T>();
			if (predicate!=null)
			{
				q = q.Where(predicate);
			}
			if (orderby !=null)
			{
				q = q.OrderBy<TValue>(orderby);
			}
			return await q.ToListAsync();
		}

		public async Task<T> Get(Expression<Func<T, bool>> predicate)
		{
			return await db.FindAsync<T>(predicate);
		}
		public AsyncTableQuery<T> AsQueryable()
		{
			
			return db.Table<T>();
		}

		public async Task<int> Insert(T Entity)
		{
				
			return await db.InsertAsync(Entity);
		}

		public async Task<int> Update(T Entity)
		{
			return await db.UpdateAsync(Entity);
		}

		public async Task<int> Delete(T Entiyy)
		{
			return await db.DeleteAsync(Entiyy);
		}

        public void Deletee(int id)
        {
            // TODO IMPORTANT DONT FORGET

            // TODO db.dele
			 
        }

		#endregion
	}
}

