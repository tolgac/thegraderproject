﻿using System;
namespace TheGrader
{
    public static class ApplicationConsts
    {
        public const int WeeklySchSceneWidth = 1000;

        public const int SessionRowStartIndex = 2; //1 based
        public const int SessionColumnStartIndex = 0; //zero Based

        public const int DayRowStartIndex = 2;
        public const int DayColumnStartIndex = 0;

        public const int BoxRowStartIndex = 3;
        public const int BoxColumnStartIndes = 0;

        public const int DragButtonColIndex = 0;
        public const int DragButtomRowIndex = 1;

        //Sets after gridsetup
        public static float ColumnWidth;
        public static float RowHeight;


    }
}
