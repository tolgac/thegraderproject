﻿using System;
namespace TheGrader
{
    public interface IFileHelper
    {
        bool FileExist(string path);
        string GetApplicationDataFolder();
        string GetCurrentDirectory();
        string MakeDir(string parentfolder,string foldername);
        string Combine(string path1, string path2);
    }
}
