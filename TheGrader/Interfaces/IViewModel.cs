﻿using System;
using Xamarin.Forms;

namespace TheGrader
{
	public enum ViewOption
	{
		None,
		View,
		AddNew,
		Edit,
	};

	public interface IViewModel
	{
		INavigation Navigation { get; set; }
	}
}

