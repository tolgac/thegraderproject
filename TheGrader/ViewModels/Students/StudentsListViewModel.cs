﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Reflection;

namespace TheGrader
{
	public class StudentsListViewModel : IViewModel, INotifyPropertyChanged
	{
		#region Private fields
		private readonly TheGraderCore.IStudentService _StudentService;
		private readonly TheGraderCore.IClassStudentService _StudentClassService;
		private readonly TheGraderCore.IPlusMinusService _PlusMinusService;
		private List<StudentModel> _Students;
		private ICommand _MinusTapCommand;
		private ICommand _PlusTapCommand;
		#endregion

        #region Properties
        public INavigation Navigation { get; set; }
        public List<StudentModel> Students {
            get { return _Students; }
            set { _Students = value; OnPropertyChanged("Students"); }
        }

        //public int ClassID { get; set;}
        public ClassModel CurrentClass { get; set; }
        #endregion

		#region Ctor
		public StudentsListViewModel( TheGraderCore.IStudentService studentService,
		                             TheGraderCore.IClassStudentService studentClassService,
		                             TheGraderCore.IPlusMinusService aPMService )
		{
			this._PlusMinusService = aPMService;
			this._StudentService = studentService;
			this._StudentClassService = studentClassService;

			Students = new List<StudentModel>();

			_PlusTapCommand = new Command( OnPlusTap );
			_MinusTapCommand = new Command( OnMinusTap );
		}
        #endregion

		#region Public Methods
		public List<StudentModel> GetStudents()
		{
			var studentsInClass =  _StudentClassService.GetStudentClass(x => x.ClassID == CurrentClass.ID);
			var e = studentsInClass.Select(x => x.StudentID);

			var students =  _StudentService.GetStudents(x=>e.Contains(x.ID));
            Students = new List<StudentModel>();
			foreach( var aStudent in students )
			{
				var aStudentModel = new StudentModel( aStudent );
				Students.Add( aStudentModel );
			}
		 
			OnPropertyChanged( "Students" );
			return  Students;
		}
		#endregion

		#region Commands
        public ICommand ToolBarHomeCommand {
            get {
                return new Command(async (obj) => {
                    await Navigation.PushAsync(new TargetExamplePage(obj.ToString()));
                });
            }
        }
        public ICommand ToolBarAttandancePageCommand {
            get {
                return new Command(async (obj) => {
                    await Navigation.PushAsync(new TargetExamplePage(obj.ToString()));
                });
            }
        }

        public ICommand ToolBarBehaivorPageCommand {
            get {
                return new Command(async (obj) => {
                    await Navigation.PushAsync(new TargetExamplePage(obj.ToString()));
                });
            }
        }

		public ICommand EditStudent
		{
			get
			{
				return new Command( async ( obj ) =>
				{
					System.Diagnostics.Debug.WriteLine( "StudentListViewModel.EditStudent method" );
					var item = obj as  StudentModel;
					if( item != null )
					{
                        await Navigation.PushAsync( new EditStudentPage( CurrentClass.ID, item   ) );
					}
				} );
			}
		}

		public ICommand PlusTapCommand { get { return _PlusTapCommand; } }
        public ICommand MinusTapCommand { get { return _MinusTapCommand; } }
        #endregion

        #region private section
        private void AddNewPlusMinus( object obj, bool bIsPlus )
        {
            var s = (obj ?? null) as TheGraderCore.Student;
            if (s != null) {
                _PlusMinusService.NewPlusMinus(new TheGraderCore.PlusMinus { ClassID = CurrentClass.ID, StudentID = s.ID, IsPlus = bIsPlus });
            }
        }

        void OnPlusTap(object obj)
        {
            AddNewPlusMinus(obj, true);
        }

        void OnMinusTap(object obj)
        {
            AddNewPlusMinus(obj, false);
        }
        #endregion

		#region Inotify
		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			var changed = PropertyChanged;
			if (changed != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		#endregion
	}
}
