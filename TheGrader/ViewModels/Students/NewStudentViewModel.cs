﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace TheGrader
{
	public class NewStudentViewModel : IViewModel, INotifyPropertyChanged
	{
        #region Private Fields
        private readonly TheGraderCore.IStudentService _StudentService;
        private readonly TheGraderCore.IClassStudentService _ClassStudentService;
        #endregion

        #region Properties
        public INavigation Navigation { get; set; }
        public TheGraderCore.Student aStudent { get; set; }
        public int aClassID { get; set; }
        #endregion

		#region Ctor
		public NewStudentViewModel( TheGraderCore.IStudentService pStudentService, 
                                   TheGraderCore.IClassStudentService pClassStudentService )
		{
			this._StudentService = pStudentService;
			this._ClassStudentService = pClassStudentService;
			aStudent = new TheGraderCore.Student();
		}
		#endregion

		#region Commands
		public ICommand SaveNewStudentToClassCommand
		{
			get
			{
				return new Command( async ( o ) =>
				{
					var isOK = await _StudentService.AddStudent( aStudent );

					if( isOK == 1 )
					{
						var cs = new TheGraderCore.Class_Student();
						cs.ClassID = aClassID;
						cs.StudentID = aStudent.ID;

						await _ClassStudentService.AddStudent( cs );
					}
					aStudent = new TheGraderCore.Student();
					OnPropertyChanged( "aStudent" );
				} );
			}
		}
		#endregion

		#region Inotify
		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			var changed = PropertyChanged;
			if (changed != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		#endregion
	}
}
