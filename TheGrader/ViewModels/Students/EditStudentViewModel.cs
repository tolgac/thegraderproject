﻿using System;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using Plugin.Media;
using Xamarin.Forms;

namespace TheGrader
{
    public class EditStudentViewModel : IViewModel, INotifyPropertyChanged
    {
        #region Private Fields
        private readonly TheGraderCore.IStudentService _StudentService;
        private readonly TheGraderCore.IPlusMinusService _PlusMinusService;
        private readonly string ImagesFullFolder;
        private const string ImageFolder = "StudentImages";
        #endregion

        #region Properties
        public INavigation Navigation { get; set; }
        public StudentModel aStudent { get; set; }
        public int aClassID { get; set; }
        public int PlusCount { get; set; }
        public int MinusCount { get; set; }
        public ImageSource StudentImage { get; set; }
        #endregion

		#region Ctor
		public EditStudentViewModel( TheGraderCore.IStudentService pStudentService, 
                                     TheGraderCore.IPlusMinusService pPlusMinusService )
		{
			this._StudentService = pStudentService;
            this._PlusMinusService = pPlusMinusService;

            var datafolder = DependencyService.Get<IFileHelper>().GetApplicationDataFolder();
            ImagesFullFolder = DependencyService.Get<IFileHelper>().MakeDir(datafolder, ImageFolder);


        }
		#endregion

        #region public methods
        public void GetPlusMinusCounts()
        {
            PlusCount = _PlusMinusService.GetPlusCount(aClassID, aStudent.ID);
            MinusCount = _PlusMinusService.GetMinusCount(aClassID, aStudent.ID);
        }

        public void GetStudentImage()
        { 
         var imgexist = DependencyService.Get<IFileHelper>().FileExist(aStudent.ImagePath);

            if (imgexist) {
                
                StudentImage = ImageSource.FromFile(aStudent.ImagePath);
            } else {
                StudentImage = ImageSource.FromResource("TheGrader.Resources.anonymous.png");

            }
        }
        #endregion

		#region Commands
        public ICommand TakePictureCommand 
        {
            get 
            {
                return new Command(async (obj) => {
                    StudentImage = await TakePicture( );
                });
            }
        }
		public ICommand UpdateStudent
		{
			get
			{
				return new Command( async ( o ) =>
				{
                    var dbstudent = aStudent.GetStudent();
                    await _StudentService.EditStudent( dbstudent);
					await this.Navigation.PopAsync( true );
				} );
			}
		}
        #endregion

        #region Private methods
        private async Task<ImageSource> TakePicture()
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported) {
                //Message camera is not available
                return null;
            }
            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions {
                Directory = ImageFolder,
                Name = string.Format("{0}_{1}.jpg", aStudent.StudentNumber, aStudent.FullName)
            });

            //incase user canceled
            if (file == null)
                return null;
            
            var imagesource = ImageSource.FromStream(() => {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });

            //Update student  
            var dbstudent = aStudent.GetStudent();
            dbstudent.ImagePath = file.Path;
            aStudent.ImagePath = file.Path;
            
            System.Diagnostics.Debug.WriteLine(aStudent.ImagePath);
            var result =await _StudentService.EditStudent(dbstudent);

            return imagesource;


        }
        #endregion
		#region Inotify
		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			var changed = PropertyChanged;
			if (changed != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		#endregion
	}
}
