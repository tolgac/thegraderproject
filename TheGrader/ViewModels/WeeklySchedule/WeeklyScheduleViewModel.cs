﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel;
using TheGraderCore;
using System.Linq;
namespace TheGrader
{
    public class WeeklyScheduleViewModel : IViewModel, INotifyPropertyChanged
    {
        #region Ctor
        public WeeklyScheduleViewModel(IWeeklyScheduleService scheduleservice, IClassService classservice)
        {


            this.scheduleservice = scheduleservice;
            this.classservice = classservice;
            SessionCount = 13;

            //kayıtlı sınıf listesi
            var domclasses = classservice.GetClassList();

            Classes = new List<ClassModel>();
            foreach (var item in domclasses) {
                var classmodel = new ClassModel(item);
                Classes.Add(classmodel);
            }

            //varsayılan sınıf
            if (Classes.Count>0) {
                SelectedClass = Classes[0];
                
            }

            //Kayıtlı sch
            Sessions = scheduleservice.GetSync();

            //Action sheet string array
            ClassArray = new string[Classes.Count];
            var i = 0;
            foreach (var item in Classes) {
                ClassArray[i] = item.FullName;
                i++;
            }
        }
        #endregion

        #region Private fields
        private readonly IWeeklyScheduleService scheduleservice;
        private readonly IClassService classservice;
        #endregion

        #region Properties
        public INavigation Navigation { get; set; }
        public int SessionCount { get; set; }

        public List<ClassModel> Classes {get;set;}
        public ClassModel SelectedClass { get; set;}

        public List<Session> Sessions { get; set; }
        public string[] ClassArray { get; set; }
        #endregion

        #region Commands

        public ICommand ToolBarHomePageCommand { 
            get {
                return new Command(async (obj) => {
                    
                    await Navigation.PushAsync(new TargetExamplePage(obj.ToString()));
                });
            }
        }
        public ICommand ToolBarClassesPageCommand {
            get {
                return new Command(async (obj) => {
                    await Navigation.PushAsync(new ClassesListPage());
                });
            }
        }
        
        public ICommand GetSessions {
            get {
                var c = new Command(() => {
                    Sessions = scheduleservice.GetSync();

                });

                return c;
            }
        }
        public ICommand DeleteSessions {
            get {
                var c = new Command(() => {
                    var dbresult = scheduleservice.GetSync();

                    foreach (var item in dbresult) {
                        scheduleservice.DeleteSceduleById(item.ID);
                        Sessions.Clear();
                    }
                });

                return c;
            }
        }
        public ICommand ChangeCurrentCalssCommand {
            get {
                return new Command((obj) => {
                    if (obj != null) {
                        var current = Classes.Where(x => x.FullName.Equals(obj)).FirstOrDefault();
                        if (current != null) {
                            SelectedClass = current;
                        }
                    }
                });
            }
            }
        public ICommand StudentListCommand {
            get {
                return new Command(async (obj) => {
                    //Öğrenici listesi, eğer öğrenci yoksa öğrenci ekleme ekranına geçiş
                    System.Diagnostics.Debug.WriteLine("StudentListCommand");
                    var classid = (int)obj;
                    if (classid != 0) {

                        var cm = Classes.FirstOrDefault(x => x.ID == classid);
                        if (cm != null) {
                            
                            await Navigation.PushAsync( new StudentsListPage(cm) );
                            
                        }
                    }
                });
            }
        }

        public ICommand SaveLayout {
            get {
                return new Command(async (obj) => {

                    //var boxlist = obj as List<WeeklySchClassItem>;
                    var btns = obj as List<DragButton>;
                    foreach (var item in btns) {
                        //Db'den gelip silinenler.
                        if (item.IsRemoved && item.IsOnDB) {
                            await this.scheduleservice.DeleteSceduleById(item.ID);
                            continue;
                        }

                        //Dbden gelip editlenenler
                        if (!item.IsRemoved && item.IsOnDB) {
                            var lesson = await scheduleservice.GetScheduleItem(item.ID);
                            if (lesson != null) {
                                lesson.DayOfWeek = item.Day;
                                lesson.LessonOrder = item.Session;
                                await scheduleservice.EditScheduleItem(lesson);
                                continue;
                            }
                        }

                        //Yeni eklenenler
                        if (!item.IsRemoved && !item.IsOnDB) {
                            var les = new Session();
                            les.ClassID = item.ClassID;
                            les.DayOfWeek = item.Day;
                            les.LessonOrder = item.Session;
                             
                            var cls = Classes.FirstOrDefault(x => x.ID.Equals(item.ClassID));
                            if (cls!=null) {
                                les.SchoolID = cls.SchoolID;
                            }
                            await scheduleservice.AddScheduleItem(les);
                            continue;
                        } 
                    }
                    Sessions = scheduleservice.GetSync();
                });
            }
        }
        #endregion

        #region Inotify
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var changed = PropertyChanged;
            if (changed != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

    }
}
