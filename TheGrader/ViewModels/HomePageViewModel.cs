﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.ComponentModel;
using System.Windows.Input;
using TheGraderCore;
using System.Linq;

namespace TheGrader
{
    public class HomePageViewModel : IViewModel, INotifyPropertyChanged
    {
        #region Private Fields
        public event PropertyChangedEventHandler PropertyChanged;
        public INavigation Navigation { get; set; }
        private ICommand _LessonsTapCommand;
        #endregion

        #region Properties
        public ICommand LessonsTapCommand { get { return _LessonsTapCommand; } }
        #endregion


        #region Public Methods
        public HomePageViewModel()
        {
            _LessonsTapCommand = new Command(OnLessonsTap );
        }

        #endregion


        #region Private Methods
        private void OnLessonsTap(object obj)
        {
            System.Diagnostics.Debug.WriteLine( "Lessons SvgImage" );
            string str = obj as string;
            if( str.Equals("Classes") )
            {
                Navigation.PushAsync(new ClassesListPage());
            }
            else if( str.Equals("Schedule") )
            {
               // Navigation.PushAsync(new WeeklySchedulePage());
                Navigation.PushAsync(new WeeklySchedulePage2());
                
            }
            else if( str.Equals("Settings") )
            {
                Navigation.PushAsync( new TargetExamplePage() );
            }
        }
        #endregion

        #region Commands
        //public ICommand AddStudentCommand {
        //    get {
        //        return new Command(async (obj) => {
        //            var item = obj as GestureLabel;
        //            if (item != null) {
        //                var cm = item.CommandParameter as TheGraderCore.Class;
        //                if (cm != null) {
        //                    await Navigation.PushAsync(new TargetExamplePage());//TODO (new AddClassPage(cm.ID));
        //                }

        //            }

        //        });
        //    }
        //}

        //public ICommand StudentListCommand {
        //    get {
        //        return new Command(async (obj) => {
        //            // TODO Öğrenici listesi, eğer öğrenci yoksa öğrenci ekleme ekranına geçiş
        //            System.Diagnostics.Debug.WriteLine("StudentListCommand");
        //            var item = obj as ClassModel;
        //            if (item != null) {
        //                await Navigation.PushAsync(new StudentsListPage(item));
        //            }
        //        });
        //    }
        //}

        //public ICommand DeleteClassCommand {
        //    get {
        //        return new Command(async (obj) => {
        //            //Sınıf sil
        //            var item = (ClassModel)obj;

        //            List<Class_Student> classStudents = await this._ClassStudentService.GetStudentsByClassID(item.ID);
        //            var e = classStudents.Select(x => x.StudentID);
        //            var students = _StudentService.GetStudents(x => e.Contains(x.ID));
        //            foreach (var aStudent in students) {
        //                await _StudentService.DeleteStudent(aStudent);
        //            }

        //            foreach (var aClass_Student in classStudents) {
        //                await _ClassStudentService.DeleteClassStudent(aClass_Student);
        //            }

        //            // var cls = await ClassService.GetClassByID(item.ID);
        //            // await this.ClassService.DeleteClass(cls);
        //            await this._ClassService.DeleteClass(item.GetClass());
        //            this.Classes = this._ClassService.GetClassList();

        //            MessagingCenter.Send<ClassesListPageViewModel>(this, "UpdateClassList");
        //            //await this.Navigation.PopAsync(true);
        //        });
        //    }
        //}

        //public ICommand EditClassCommand {
        //    get {
        //        return new Command(async (obj) => {
        //            var item = obj as ClassModel;

        //            // TODO editclass.SelectedSchool = await SchoolService.GetSchoolByID(item.SchoolID);
        //            await this.Navigation.PushAsync(new TargetExamplePage());//TODO (new EditClassPage(editclass), true);
        //            //Sınıf düzenleme ekranına geçiş
        //            //await this.Navigation.PopAsync(true);
        //        });
        //    }
        //}
        #endregion

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }
    }

}
