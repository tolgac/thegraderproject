﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel;

namespace TheGrader
{
	public class AddClassViewModel : IViewModel,INotifyPropertyChanged
	{
		#region private fields
		private readonly TheGraderCore.IClassService Classservice;
		int _index;
		#endregion


		#region Ctor
		public AddClassViewModel( TheGraderCore.IClassService classservice )
		{
			this.Classservice = classservice;
		}
		#endregion

		#region properties
		public INavigation Navigation { get; set; }
		public string NewClassSchoolName { get; set; }
		public string NewClassName { get; set; }
		public string NewClassGrade { get; set;}
		public string NewClassBranch { get; set;}
		#endregion 
				 
		#region Commands
		public ICommand SaveNewClassCommand 
		{
			get
			{
				return new Command(async (o) =>
				{
					var aClass = new TheGraderCore.Class();
					aClass.ClassName = NewClassName;
					aClass.Branch = NewClassBranch;
					aClass.Grade = NewClassGrade;
					aClass.SchoolName = NewClassSchoolName;
//TODO					d.SchoolID = this.SelectedSchool.ID;
					
					await Classservice.AddClass(aClass);
					await this.Navigation.PopAsync(true);
				});
			}
		}

		public int SelectIndex
		{
			get{return _index;}
			set
			{
				_index = value;
				OnPropertyChanged("SelectIndex");
			}
		}
		#endregion

 
		#region Inotify
		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			var changed = PropertyChanged;
			if (changed != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		#endregion

	}
}

