﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.ComponentModel;
using System.Windows.Input;
using TheGraderCore;
using System.Linq;

namespace TheGrader
{
	public class ClassesListPageViewModel : IViewModel  ,INotifyPropertyChanged
	{ 
		// Member variables
        private readonly IClassService _ClassService;
        private readonly IClassStudentService _ClassStudentService;
        private readonly IStudentService _StudentService;
		public event PropertyChangedEventHandler PropertyChanged;
		private List<TheGraderCore.Class> _Classes;
		public INavigation Navigation { get; set; }

		// Member properties

		//DTO kullanılabilir
		//public List<Domain.Class> Classes 
		//{
		//	get;
		//	set;
		//}
		public List<TheGraderCore.Class> Classes
		{
			get { return _Classes; }
			set { _Classes = value; OnPropertyChanged( "Classes" ); }
		}

		// Member functions
		public  ClassesListPageViewModel( IClassService classService,

                                   // TODO classStudent ve student servisleri yerine ICleaningDB servisi eklenecek
                                   IClassStudentService classStudentService,
                                   IStudentService studentService )
		{ 
			this._ClassService = classService;
            this._ClassStudentService = classStudentService;
            this._StudentService = studentService;
			this._Classes = this._ClassService.GetClassList();
		}

		public List<TheGraderCore.Class> UpdateClassesList()
		{
			this._Classes = this._ClassService.GetClassList();
			return this._Classes;
		}

		public List<ClassModel> GetClassModelsList()
		{
			var classes = UpdateClassesList();
			var result = new List<ClassModel>();

			foreach( var aClass in classes )
			{
				var aClassModel = new ClassModel( aClass );
				result.Add( aClassModel );
			}

			return result;
		}

		#region Commands
        public ICommand ToolBarHomeCommand {
            get {
                return new Command(async (obj) => {
                    await Navigation.PushAsync(new TargetExamplePage(obj.ToString()));
                });
            }
        }
        public ICommand WeeklyScheduleCommand {
            get {
                return new Command(async (obj) => {
                    await Navigation.PushAsync(new WeeklySchedulePage());
                });
            }
        }

        public ICommand ImportClassPageCommand {
            get {
                return new Command(async (obj) => {
                    await Navigation.PushAsync(new TargetExamplePage(obj.ToString()));
                });
            }
        }

		public ICommand AddStudentCommand
		{
			get
			{
				return new Command(async (obj) => {
					var item = obj as GestureLabel;
					if (item!=null)
					{
						var cm = item.CommandParameter as TheGraderCore.Class;
						if (cm!=null)
						{
							await Navigation.PushAsync( new TargetExamplePage() );//TODO (new AddClassPage(cm.ID));
						}

					}

				});
			}
		}

		public ICommand StudentListCommand
		{
			get
			{
				return new Command( async ( obj ) =>
				{
					// TODO Öğrenici listesi, eğer öğrenci yoksa öğrenci ekleme ekranına geçiş
					System.Diagnostics.Debug.WriteLine( "StudentListCommand" );
					var item = obj as ClassModel;
					if( item != null )
					{
						await Navigation.PushAsync( new StudentsListPage( item ) );
					}
				} );
			}
		}

		public ICommand DeleteClassCommand
		{
			get{
                return new Command(async (obj) => {
                    //Sınıf sil
                    var item = (ClassModel)obj;

                    List<Class_Student> classStudents = await this._ClassStudentService.GetStudentsByClassID(item.ID);
                    var e = classStudents.Select(x => x.StudentID);
                    var students = _StudentService.GetStudents(x => e.Contains(x.ID));
                    foreach (var aStudent in students) {
                        await _StudentService.DeleteStudent( aStudent );
                    }

                    foreach( var aClass_Student in classStudents ) {
                        await _ClassStudentService.DeleteClassStudent(aClass_Student);
                    }

                    // var cls = await ClassService.GetClassByID(item.ID);
                    // await this.ClassService.DeleteClass(cls);
                    await this._ClassService.DeleteClass(item.GetClass());
                    this.Classes = this._ClassService.GetClassList();

                    MessagingCenter.Send<ClassesListPageViewModel>(this, "UpdateClassList");
                    //await this.Navigation.PopAsync(true);
                });
			}
		}

		public ICommand EditClassCommand
		{
			get{
				return new Command(async (obj) => {
                    var item = obj as ClassModel;

					// TODO editclass.SelectedSchool = await SchoolService.GetSchoolByID(item.SchoolID);
					await this.Navigation.PushAsync( new TargetExamplePage() );//TODO (new EditClassPage(editclass), true);
					//Sınıf düzenleme ekranına geçiş
					//await this.Navigation.PopAsync(true);
				});
			}
		}
		#endregion
		 
		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this,
					new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}

