using System;

using Xamarin.Forms;
using System.Reflection;

namespace TheGrader
{
	public class TheGraderApp : Application
	{
		public TheGraderApp( AppSetup setup )
		{
			AppContainer.Container = setup.CreateContainer();
			// var aHomePage = new ClassesListPage();
            var aHomePage = new HomePage();
			MainPage = new NavigationPage( aHomePage );
   //         var weekly = new WeeklySchedulePage();
			//MainPage = new NavigationPage( weekly );
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
