﻿using System;
using System.Collections.Generic;
using System.Reflection;
using SVG.Forms.Plugin.Abstractions;
using Xamarin.Forms;
using System.Linq;
namespace TheGrader
{
    public static class StaticData
    {
        public static List<string> Days {
            get {
                return new List<string>() { "Pzt", "Sal", "Çrş", "Prş", "Cma", "Cts", "Pzr", "" };
            }
        }
    }

    public class WeeklySchedulePage:ViewPage<WeeklyScheduleViewModel>
    {
        #region Ctor
        public WeeklySchedulePage()
        {
            ViewModel.Navigation = this.Navigation;
            boxes = new List<WeeklySchBox>();
            HourLabels = new List<Label>();
            AssingedBoxes = new List<WeeklySchClassItem>();
            
            SetupGrid();
            AddHoursColumn();
            AddWeekDayRow();
            AddBoxes();
            AddDragButton();
            AddSaveButton();
            AddClearButton();
            BindingContext = ViewModel;

            var tlbhomepage = new MenuToolBarItem(ViewModel.ToolBarHomePageCommand, "TheGrader.Resources.Home.svg","Ana Sayfa","Ana Sayfa");
            var tlbClasses = new MenuToolBarItem(ViewModel.ToolBarClassesPageCommand, "TheGrader.Resources.siniflar-01.svg", "Sınıflar", "Sınıflar");
             
            var bottomtoolbar = new MenuToolBar(new List<MenuToolBarItem>() { tlbhomepage, tlbClasses });


            
            var scroll = new ScrollView {
                Content = WeeklyGrid,
                Orientation = ScrollOrientation.Vertical,
            };

            scroll.Content = WeeklyGrid;
            var stack = new StackLayout { 
                 Padding = new Thickness(10, 10, 10,10),
            };
            stack.Children.Add(scroll); 

            stack.Children.Add(bottomtoolbar);
            
            Content = stack;

        }
        #endregion

        #region private fields
        private Grid WeeklyGrid;
        private WeeklySchClassItem DragButton;
        private List<WeeklySchBox> boxes;
        private List<Label> HourLabels;
        private List<WeeklySchClassItem> AssingedBoxes;

        #endregion

        #region Events
        async void OnPanUpdated(object sender, PanUpdatedEventArgs e)
        {
            //deneme

            var q = sender as WeeklySchClassItem;
            switch (e.StatusType) {
                case GestureStatus.Started:

                    //başladığı spottan ilişkisini kesiyoruz.
                    var leftspot = boxes.Where(x => x.Day == q.Day).Where(x => x.Session == q.Session).FirstOrDefault();
                    if (leftspot != null) {
                        leftspot.Lesson = null;

                    }
                    break;

                case GestureStatus.Running:
                    q.TranslationX = q.XX + e.TotalX;
                    q.TranslationY = q.YY + e.TotalY;
                    break;
                case GestureStatus.Completed:
                    //hangi spota gittiğini bul
                    //**************

                    var q_rect = new Rectangle((q.X + q.TranslationX), (q.Y + q.TranslationY), q.Width, q.Height);

                    var spot = boxes.FirstOrDefault(x => x.IsInsight(q_rect.Center));
                    if (spot != null) {
                        
                        //daha önceden burada atanmış varmı
                        if (spot.Lesson != null && spot.Lesson != q) {

                            WeeklyGrid.Children.Remove(spot.Lesson); 
                        }

                        if (q.X == float.Epsilon && q.Y == float.Epsilon) {
                            q.XX = q.X + q.TranslationX;
                            q.YY = q.Y + q.TranslationY;

                        } else {
                            q.XX = q.TranslationX;
                            q.YY = q.TranslationY;

                        } 

                         //Kutuya yerleştir. Translateto relative çalışıyor.
                        var xsign = 1;
                        var ysign = 1;
                        if (q.X > spot.X) {
                            xsign = -1;
                        }
                        if (q.Y > spot.Y) {
                            ysign = -1;
                        }

                         await q.TranslateTo(xsign * Math.Abs(q.X - spot.X), ysign * Math.Abs(q.Y - spot.Y));

                        
                        //Gün ve ders
                        q.Day = spot.Day;
                        q.Session = spot.Session ; 

                        //Kutuya kim olduğunu ekle
                        spot.Lesson =null;
                        spot.Lesson = q; 
                         
                        if (!q.IsOnDB) {
                            AssingedBoxes.Add(q);
                            AddDragButton();
                            //atanmış kutu listesine ekle. kayıt etmek için laazım.

                        }
                        q.IsAssigned = true;
                       
                       

                    } else {
                        //Orjinal yerine
                        await q.TranslateTo(WeeklyGrid.X, WeeklyGrid.Y);

                        if (q.IsAssigned) {
                            
                            if (!q.IsOnDB) {
                                AssingedBoxes.Remove(q);

                            }
                            q.IsRemoved = true; 

                            WeeklyGrid.Children.Remove(q);

                        }
                    }
                    break;


            }
        }
        #endregion
        #region Private methods
        void AddSavedData()
        {

            foreach (var item in ViewModel.Sessions) {
                var clss = ViewModel.Classes.FirstOrDefault(x => x.ID.Equals(item.ClassID));
                if (clss != null) {
                    var lessonbox = new WeeklySchClassItem(clss.ShortClassName);
                    lessonbox.Caption = clss.ShortClassName;
                    lessonbox.IsAssigned = true;
                    lessonbox.ClassID = clss.ID;
                    lessonbox.Day = item.DayOfWeek;
                    lessonbox.Session = item.LessonOrder;
                    lessonbox.IsOnDB = true;
                    lessonbox.ID = item.ID;

                    PanGestureRecognizer pangesture = new PanGestureRecognizer();
                    pangesture.PanUpdated += OnPanUpdated;

                    TapGestureRecognizer tapgesture = new TapGestureRecognizer();
                    tapgesture.Tapped += (sender, e) => {
                        //Open class list
                        var box = sender as WeeklySchClassItem;
                        ViewModel.StudentListCommand.Execute(box.ClassID);

                    };

                    lessonbox.GestureRecognizers.Add(tapgesture);
                    lessonbox.GestureRecognizers.Add(pangesture);

                    AssingedBoxes.Add(lessonbox);

                    var boxx = boxes.Where(x => x.Day.Equals(item.DayOfWeek)).Where(x => x.Session.Equals(item.LessonOrder)).FirstOrDefault();
                    if (boxx != null) {
                        boxx.Lesson = lessonbox;
                    }
                    WeeklyGrid.Children.Add(lessonbox, lessonbox.Day, lessonbox.Session + 1);


                }

            }
        }
        void SetupGrid()
        {
            WeeklyGrid = new Grid { 
                ColumnSpacing =1,
                RowSpacing =1,
                VerticalOptions = LayoutOptions.Fill
            
            };


            for (int i = 0; i < ViewModel.SessionCount+1; i++) {
                WeeklyGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                
            }

            for (int i = 0; i < 5; i++) {
                WeeklyGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            }

        }

        void AddHoursColumn()
        { 
            for (int i = 1; i < ViewModel.SessionCount - 2; i++) {
                    var lbl = new Label {
                        Text = i + "",
                        Style = GridStyles.HoursLabelStyle,
                        HorizontalOptions = LayoutOptions.Center,
                        VerticalOptions = LayoutOptions.Center
                    };
                    WeeklyGrid.Children.Add(lbl, 0, i + 1);
                    HourLabels.Add(lbl);

                }
        }

        void AddWeekDayRow()
        { 
            var days = StaticData.Days;
            var i = 1;
            foreach (var item in days) {
                WeeklyGrid.Children.Add(new Label {

                    Text = item,
                    Style = GridStyles.DaysLabelStyle
                }, i, 1);//0'nı rowu boş bırakıyoruz
                i++;
            }
        }

        void AddBoxes()
        { 
            for (int i = 1; i < 6; i++) {
                for (int j = 2; j < ViewModel.SessionCount - 1; j++) {

                    var box = new WeeklySchBox(" ") {
                        VerticalOptions = LayoutOptions.Center,
                        Day = i,
                        Session = j-1,
                        Caption = " " + Environment.NewLine

                    };
                    WeeklyGrid.Children.Add(box, i, j);

                    boxes.Add(box);

                }
            }
        }

        WeeklySchClassItem AddDragButton()
        { 
            DragButton = new WeeklySchClassItem("11A Mat");
            DragButton.Caption = ViewModel.SelectedClass.ShortClassName;
            DragButton.ClassID = ViewModel.SelectedClass.ID;
            DragButton.IsOnDB = false;
            DragButton.ID = -1;
        
            PanGestureRecognizer pangesture = new PanGestureRecognizer();
            pangesture.PanUpdated += OnPanUpdated;
            DragButton.GestureRecognizers.Add(pangesture);

            TapGestureRecognizer tapgesture = new TapGestureRecognizer();
            tapgesture.Tapped += async (sender, e) => {
                //Open class list
                var box = sender as WeeklySchClassItem;

                if (box.Day != 0 && box.Session != 0) {
                    //goto class list
                    ViewModel.StudentListCommand.Execute(box.ClassID);
                } else {
                    var action = await DisplayActionSheet("Sınıflar", "İptal", null, ViewModel.ClassArray);
                    ViewModel.ChangeCurrentCalssCommand.Execute(action);
                    DragButton.Caption = ViewModel.SelectedClass.ShortClassName;
                    DragButton.ClassID = ViewModel.SelectedClass.ID;

                }

                //HighLightLessons();

            };
            DragButton.GestureRecognizers.Add(tapgesture);
            WeeklyGrid.Children.Add(DragButton, 0, 1, 0, 1);
            Grid.SetColumnSpan(DragButton, 1);
            //HighLightLessons();
            return DragButton;

        }

        void AddSaveButton()
        { 
            var imgSave = new SvgImage {
                SvgPath = "TheGrader.Resources.flopy.svg",
                //SvgPath = "TheGrader.Resources.presentation-01.svg",
                
                SvgAssembly = this.GetType().GetTypeInfo().Assembly,
                WidthRequest = 32,
                HeightRequest = 32,
                MinimumWidthRequest=32
                    
            };

            TapGestureRecognizer tapgesture = new TapGestureRecognizer();
            tapgesture.Command = ViewModel.SaveLayout;
            tapgesture.CommandParameter = AssingedBoxes;
            //tapgesture.Tapped += (sender, e) => {
                
            //    if (ViewModel.SaveLayout.CanExecute(AssingedBoxes)) {
            //        ViewModel.SaveLayout.Execute(AssingedBoxes);
            //    }
               
            //};
            imgSave.GestureRecognizers.Add(tapgesture);
            var clockstack = new StackLayout {
                Padding = new Thickness(0, 5, 0, 0),
                Children = { imgSave }

            };

            WeeklyGrid.Children.Add(clockstack, 5, 0);

        }

        void AddClearButton()
        { 
            var imgClear = new SvgImage {
                SvgPath = "TheGrader.Resources.clear.svg",
                SvgAssembly = this.GetType().GetTypeInfo().Assembly,
                WidthRequest = 32,
                HeightRequest = 32
            };

            TapGestureRecognizer tapgesture = new TapGestureRecognizer();
            tapgesture.Tapped += (sender, e) => {
                foreach (var item in AssingedBoxes) {
                    WeeklyGrid.Children.Remove(item); 
                }
                if (ViewModel.DeleteSessions.CanExecute(null)) {
                    ViewModel.DeleteSessions.Execute(null);
                }

            };

            imgClear.GestureRecognizers.Add(tapgesture);
            var clockstack = new StackLayout {
                Padding = new Thickness(0, 5, 0, 0),
                Children = { imgClear }

            };
            WeeklyGrid.Children.Add(clockstack, 4, 0);
            
        }
        #endregion

        #region overrides
        protected override void OnAppearing()
        {

            ViewModel.GetSessions.Execute(null);
            AddSavedData(); 
            base.OnAppearing(); 

        }
        #endregion
    }
}
