﻿using System;
using Xamarin.Forms;
using CocosSharp;
using System.Linq;
namespace TheGrader
{
    public class WeeklySchedulePage2 : ViewPage<WeeklyScheduleViewModel>
    {
       

        #region Ctor
        public WeeklySchedulePage2()
        {
            ViewModel.Navigation = this.Navigation;

            var gameview = new CocosSharpView() {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                ViewCreated = HandleViewCreated,

            };

            var stack = new StackLayout() {
                HorizontalOptions = LayoutOptions.Fill,
                VerticalOptions = LayoutOptions.Fill,
                BackgroundColor = Color.White,
                Children = { gameview }
            };
            Content = stack;

        }
        #endregion

        #region overrides
        protected override void OnAppearing()
        {

            ViewModel.GetSessions.Execute(null);
          
            base.OnAppearing();

        }
        #endregion
       
        #region Private fields
        private WeeklyScheduleScene _scene;

        #endregion

        #region private methods
        void HandleViewCreated(object sender, EventArgs e)
        {
            var gameview = sender as CCGameView;
            if (gameview != null) {

                var _w = this.Width;
                var _h = this.Height;
                var _desireW = ApplicationConsts.WeeklySchSceneWidth;

                var _desireH = (_h / _w) * _desireW;
                gameview.DesignResolution = new CCSizeI(_desireW, (int)_desireH);
                gameview.ResolutionPolicy = CCViewResolutionPolicy.ShowAll;

                _scene = new WeeklyScheduleScene(gameview,ViewModel.StudentListCommand);

                gameview.RunWithScene(_scene);
                if (_scene != null) { 
                    
                    AddDragButton();
                    AddSavedData();
                    AddSaveButton();
                } 
            }

        }

        private void AddSavedData()
        {
            
            foreach (var item in ViewModel.Sessions) {
                var clss = ViewModel.Classes.FirstOrDefault(x => x.ID.Equals(item.ClassID));
                if (clss != null) {

                      _scene.BaseLayer.AddSessionItem(item.DayOfWeek, item.LessonOrder, clss.ShortClassName, true, clss.ID, item.ID);
                     
                }
            }
        }

        private void AddSaveButton()
        {
            var savebtn=_scene.BaseLayer.AddSaveButton(6, 1, "");
            savebtn.TapCommand = ViewModel.SaveLayout;
            savebtn.CommandParameter = _scene.BaseLayer.AssignedBoxes;

           //_scene.BaseLayer.AddTrashButton(5, 1);
        }

        private void AddDragButton()
        { 
            var drg = _scene.BaseLayer.AddDragButton(ApplicationConsts.DragButtonColIndex, ApplicationConsts.DragButtomRowIndex, ViewModel.SelectedClass.ShortClassName,ViewModel.SelectedClass.ID);
            drg.TapCommand = new Command(async (obj) => {
                var action = await DisplayActionSheet("Sınıflar", "İptal", null, ViewModel.ClassArray);
                ViewModel.ChangeCurrentCalssCommand.Execute(action);
                drg.Label = ViewModel.SelectedClass.ShortClassName;
                drg.ClassID = ViewModel.SelectedClass.ID;

            });
            drg.Tapped += async (sender, e) => { 
             var action = await DisplayActionSheet("Sınıflar", "İptal", null, ViewModel.ClassArray);
                ViewModel.ChangeCurrentCalssCommand.Execute(action);
                drg.Label = ViewModel.SelectedClass.ShortClassName;
                drg.ClassID = ViewModel.SelectedClass.ID;
            };
          //  drg.PanCompleted += Drg_PanCompleted;

        }

        #endregion

        
    }
}
