﻿using System;
using Xamarin.Forms;
using TheGrader;
using SVG.Forms.Plugin.Abstractions;
using System.Reflection;
using Autofac.Util;
using System.Windows.Input;

namespace TheGrader
{
    public class HomePage : ViewPage<HomePageViewModel>
    {
        #region Private Fields
        private Image _LessonsImage;
        #endregion

        #region Override
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
        #endregion

        public HomePage()
        {
            WriteResources();

            ViewModel.Navigation = this.Navigation;
            Title = "Öğretmen Ajandası";

            //Button lessonsButton = new Button {
            //    HorizontalOptions = LayoutOptions.Start,
            //    VerticalOptions = LayoutOptions.Start,
            //    Text = "Dersler",
            //    WidthRequest = 120,
            //    HeightRequest = 120,
            //    BackgroundColor = Color.Gray,
            //    //Image = (FileImageSource)_LessonsImage.Source,
            //};

            //_LessonsImage = new SvgImage {
            //    HorizontalOptions = LayoutOptions.Start,
            //    VerticalOptions = LayoutOptions.Start,
            //    SvgPath = "TheGrader.Resources.Plus.svg",
            //    SvgAssembly = this.GetType().GetTypeInfo().Assembly,
            //    HeightRequest = 100,
            //    WidthRequest = 100,
            //    // BackgroundColor = Color.Black
            //};
            //var lessonsTapRecognizer = new TapGestureRecognizer { Command = ViewModel.LessonsTapCommand, NumberOfTapsRequired = 1 };
            //lessonsTapRecognizer.SetBinding(TapGestureRecognizer.CommandParameterProperty, ".");
            //_LessonsImage.GestureRecognizers.Add(lessonsTapRecognizer);

            // Asagisi calisiyordu bea
            //var lessonsImageButton = new SvgImageButton() {
            //    HorizontalOptions = LayoutOptions.Start,
            //    VerticalOptions = LayoutOptions.Start,
            //    SvgPath = "TheGrader.Resources.Plus.svg",
            //    SvgAssembly = this.GetType().GetTypeInfo().Assembly,
            //    HeightRequest = 100,
            //    WidthRequest = 100,
            //    Command = ViewModel.LessonsTapCommand,
            //    CommandParameter = ".",
            //    BackgroundColor = Color.Aqua,
            //};
            //var scheduleImageButton = new SvgImageButton() {
            //    HorizontalOptions = LayoutOptions.Start,
            //    VerticalOptions = LayoutOptions.Start,
            //    SvgPath = "TheGrader.Resources.tt.svg",
            //    SvgAssembly = this.GetType().GetTypeInfo().Assembly,
            //    HeightRequest = 100,
            //    WidthRequest = 100,
            //    Command = ViewModel.LessonsTapCommand,
            //    CommandParameter = ".",
            //    BackgroundColor = Color.Gray,
            //};

            var firstLine = new HorizontalTwoButtonLayout(100, 100);
            // firstLine.SetFirstSvgButton( "TheGrader.Resources.Plus.svg", "Ders Listesi", ViewModel.LessonsTapCommand, "Classes" );
            firstLine.SetFirstSvgButton("TheGrader.Resources.svg.034-library.svg", "Ders Listesi", ViewModel.LessonsTapCommand, "Classes");
            firstLine.SetSecondSvgButton("TheGrader.Resources.Calendar.svg", "Ders Planı", ViewModel.LessonsTapCommand, "Schedule");

            var secondLine = new HorizontalTwoButtonLayout(100, 100);
            secondLine.SetFirstSvgButton("TheGrader.Resources.svg.149-cog.svg", "Ayarlar", ViewModel.LessonsTapCommand, "Settings");
            // secondLine.SetSecondSvgButton("TheGrader.Resources.svg.270-cancel-circle.svg", "", ViewModel.LessonsTapCommand, "NA");

            Content = new StackLayout {
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.Start,
                Children = { firstLine.GetView, secondLine.GetView }, // {  lessonsImageButton, scheduleImageButton },
                Spacing = 20,
                Padding = 10,
            };
        }

        #region private methods
        private void LoadImages()
        {
            _LessonsImage = new SvgImage {
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
                SvgPath = "TheGrader.Resources.Lessons.svg",
                SvgAssembly = this.GetType().GetTypeInfo().Assembly,
                HeightRequest = 100,
                WidthRequest = 100,
                // BackgroundColor = Color.Black
            };
        }
        #endregion

        void WriteResources()
        {
            System.Diagnostics.Debug.WriteLine(">>> Embedded resources: Application ");
            var assembly = this.GetType().GetTypeInfo().Assembly;
            System.Diagnostics.Debug.WriteLine(">>> Embedded resources: assembly.fullname: " + assembly.FullName);
            System.Diagnostics.Debug.WriteLine(">>> Embedded resources: Application " + assembly.GetLoadableTypes());
            foreach (var res in assembly.GetManifestResourceNames()) {
                System.Diagnostics.Debug.WriteLine(">>> " + res);
            }
            //
            System.Diagnostics.Debug.WriteLine("\n\n>>> Embedded resources. HomePage");
            assembly = this.GetType().GetTypeInfo().Assembly;
            foreach (var res in assembly.GetManifestResourceNames()) {
                System.Diagnostics.Debug.WriteLine(">>> " + res);
            }
            ////
            //System.Diagnostics.Debug.WriteLine("\n\n>>> Embedded resources. TheGraderCore.Class");
            //assembly = typeof( TheGraderCore.Class ).GetType().GetTypeInfo().Assembly;
            //foreach( var res in assembly.GetManifestResourceNames() )
            //{
            //  System.Diagnostics.Debug.WriteLine( ">>> " + res );
            //}
            ////
            //System.Diagnostics.Debug.WriteLine("\n\n>>> Embedded resources. DBCore.ClassService");
            //assembly = typeof( DBCore.ClassService ).GetType().GetTypeInfo().Assembly;
            //foreach( var res in assembly.GetManifestResourceNames() )
            //{
            //  System.Diagnostics.Debug.WriteLine( ">>> " + res );
            //}
        }
    }

    public class HorizontalTwoButtonLayout : StackLayout
    {
        #region Private Fields
        private StackLayout _aStackLayout;
        private int _Height;
        private int _Width;
        private View _First;
        private View _Second;
        #endregion

        public View GetView {
            get {
                if( null == _aStackLayout )
                {
                    _aStackLayout = new StackLayout {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Start,
                        Spacing = 30,
                        Padding = 15,
                        // BackgroundColor = Color.Black,
                    };

                    if(null != _First) _aStackLayout.Children.Add( _First );
                    if(null != _Second) _aStackLayout.Children.Add( _Second );
                }
                return _aStackLayout;
            }
        }

        #region Constructor
        public HorizontalTwoButtonLayout( int height, int width )
        {
            this._Height = height;
            this._Width = width;

        }
        #endregion

        public void SetFirstSvgButton( string embeddedName, string label, ICommand buttonCommand, object cmdParameter )
        {
            SetSvgButton(out _First, embeddedName, label, buttonCommand, cmdParameter);
        }

        public void SetSecondSvgButton(string embeddedName, string label, ICommand buttonCommand, object cmdParameter)
        {
            SetSvgButton(out _Second, embeddedName, label, buttonCommand, cmdParameter);
        }

        private void SetSvgButton(out View param, string embeddedName, string label, ICommand buttonCommand, object cmdParameter)
        {
            var svgImg = new SvgImageButton() {
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
                SvgPath = embeddedName,
                SvgAssembly = this.GetType().GetTypeInfo().Assembly,
                HeightRequest = _Height,
                WidthRequest = _Width,
                Command = buttonCommand,
                CommandParameter = cmdParameter,
                // BackgroundColor = Color.Aqua,
            };

            var aLabel = new Label() {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.Start,
                Text = label,
                HeightRequest = 20,
                WidthRequest = _Width,
                // BackgroundColor = Color.Fuchsia,
            };

            param = new StackLayout() {
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Spacing = 0,
                Padding = 1,
                Children = { svgImg, aLabel }
            };
        }
    }
}
