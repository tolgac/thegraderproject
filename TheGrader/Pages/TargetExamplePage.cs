﻿using System;

using Xamarin.Forms;

namespace TheGrader
{
	public class TargetExamplePage : ContentPage
	{
		public TargetExamplePage()
		{
			Content = new StackLayout
			{
				Children = {
					new Label { Text = "Hello TargetExamplePage" }
				}
			};
		}

		public TargetExamplePage( string s )
		{
			Content = new StackLayout
			{
				Children = {
					new Label { Text = "Hello TargetExamplePage: " + s }
				}
			};
		}
	}
}

