﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using SVG.Forms.Plugin.Abstractions;
using System.Reflection;
using Autofac.Util;
using DBCore;

namespace TheGrader
{
	public class StudentsListPage : ViewPage<StudentsListViewModel>
	{
		#region Private fields
        private ListView _StudentsListView;
		#endregion

		#region Ctor
		public StudentsListPage( ClassModel currentClass )
		{
            Title = currentClass.GradeBranch + " " + currentClass.ClassName;
            ViewModel.CurrentClass = currentClass;
            ViewModel.Navigation = this.Navigation;

			_StudentsListView = new ListView { HorizontalOptions = LayoutOptions.CenterAndExpand };
			_StudentsListView.ItemTemplate = new DataTemplate( () => { return new StudentListViewCell( ViewModel ); } );
			_StudentsListView.RowHeight = StudentListViewCell.RowHeight;
            _StudentsListView.ItemSelected += (sender, e) => { ViewModel.EditStudent.Execute(e.SelectedItem);  };

			ToolbarItems.Add( new ToolbarItem( "Add", "", async () => { await Navigation.PushAsync( new NewStudentPage( currentClass.ID ) ); } ) );

			MessagingCenter.Subscribe<StudentsListViewModel>( this, "UpdateStudentList", 
                                                             ( obj ) => { _StudentsListView.ItemsSource = ViewModel.GetStudents(); } );
           
            var tlbhomepage = new MenuToolBarItem(ViewModel.ToolBarHomeCommand, "TheGrader.Resources.Home.svg", "Ana Sayfa", "Ana Sayfa");
            var tlbAttandance = new MenuToolBarItem(ViewModel.ToolBarAttandancePageCommand, "TheGrader.Resources.yoklama.svg", "Yoklama", "Yoklama");
            var tlbBehaivor = new MenuToolBarItem(ViewModel.ToolBarAttandancePageCommand, "TheGrader.Resources.percentages.svg", "Davranış", "Davranış");
            
            var bottomtoolbar = new MenuToolBar(new List<MenuToolBarItem>() { tlbhomepage, tlbAttandance,tlbBehaivor });



			Content = new StackLayout
			{
                Padding = new Thickness(10, 10, 10, 10),
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Children = { _StudentsListView,bottomtoolbar }
			};
		}
		#endregion

		#region Override
		protected override void OnAppearing()
		{
			base.OnAppearing();
            ViewModel.GetStudents();

            _StudentsListView.ItemsSource = ViewModel.Students;
		}
		#endregion

	}


	public class StudentListViewCell : GestureViewCell
	{
		public const int RowHeight = 30;

		public StudentListViewCell(StudentsListViewModel vm)
		{
			this.BindingContext = vm;

			var studentNameLabel = new Label
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
				FontSize = Device.GetNamedSize( NamedSize.Medium, typeof( Label ) ),
				FontAttributes = FontAttributes.None,
				HeightRequest = 20,
				// BackgroundColor = Color.Yellow,
			};
			studentNameLabel.SetBinding( Label.TextProperty, "FullName" );

			var studentNrLabel = new Label
			{
                HorizontalOptions = LayoutOptions.Fill,
                VerticalOptions = LayoutOptions.Fill,
                FontSize = Device.GetNamedSize( NamedSize.Medium, typeof( Label ) ),
				FontAttributes = FontAttributes.None,
				HeightRequest = 20,
				WidthRequest = 40,
				// BackgroundColor = Color.Purple,
			};
			studentNrLabel.SetBinding( Label.TextProperty, "StudentNumber" );

			var plusSignSvg = new SvgImage
			{
				HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
				SvgPath = "TheGrader.Resources.Plus.svg",
				SvgAssembly = this.GetType().GetTypeInfo().Assembly,
				HeightRequest = 24,
				WidthRequest = 24,
				// BackgroundColor = Color.Black
			};
			var plusTapRecognizer = new TapGestureRecognizer { Command = vm.PlusTapCommand, NumberOfTapsRequired = 1};
			plusTapRecognizer.SetBinding( TapGestureRecognizer.CommandParameterProperty, "." );
			plusSignSvg.GestureRecognizers.Add( plusTapRecognizer );

			var minusSignSvg = new SvgImage
			{
				HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Start,
				SvgPath = "TheGrader.Resources.Minus.svg",
				SvgAssembly = this.GetType().GetTypeInfo().Assembly,
				HeightRequest = 24,
				WidthRequest = 24,
			};
			var minusTapRecognizer = new TapGestureRecognizer { Command = vm.MinusTapCommand, NumberOfTapsRequired = 1};
			minusTapRecognizer.SetBinding( TapGestureRecognizer.CommandParameterProperty, "." );
			minusSignSvg.GestureRecognizers.Add( minusTapRecognizer );

			View = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Spacing = 2,
				Padding = 3,
				Children = { plusSignSvg, studentNrLabel, studentNameLabel, minusSignSvg },
			};


		}
	}
}
