﻿using System;
using Xamarin.Forms;
using SVG.Forms.Plugin.Abstractions;
using System.Reflection;
using ImageCircle.Forms.Plugin.Abstractions;

namespace TheGrader
{
	public class EditStudentPage : ViewPage<EditStudentViewModel>
	{
		#region private fields
		private Entry _TxtNameEntry;
		private Entry _TxtLastNameEntry;
		private Entry _TxtNumberEntry;
		private Button _DoneButton;
        private CircleImage _StudentImage;
        private Button _TakePicture;
		#endregion

		#region override
		protected override void OnAppearing()
		{
			base.OnAppearing();
           
        }
        #endregion

        #region Ctor
		public EditStudentPage( int pClassID, StudentModel pStudent )
		{
            Title = "Update Student";
            ViewModel.Navigation = this.Navigation;
            ViewModel.aClassID = pClassID;
            ViewModel.aStudent = pStudent;
            ViewModel.GetPlusMinusCounts();
            ViewModel.GetStudentImage();

            //Fields
            _StudentImage = new CircleImage { 
               BorderColor = Color.Black,
                FillColor = Color.Transparent,
                BorderThickness = 1,
                HeightRequest = 100,
                WidthRequest= 100,
                Aspect= Aspect.AspectFill,
                HorizontalOptions = LayoutOptions.Center
            }; 
             
            _StudentImage.SetBinding(CircleImage.SourceProperty, "StudentImage");

            _TakePicture = new Button { Text = "Resim Çek", HorizontalOptions = LayoutOptions.Center };
            _TakePicture.SetBinding(Button.CommandProperty, "TakePictureCommand");
            _TakePicture.CommandParameter = ViewModel.aStudent;

            _TxtNameEntry = new Entry { Placeholder = "First Name", VerticalOptions = LayoutOptions.Center };
            _TxtNameEntry.SetBinding(Entry.TextProperty, "aStudent.Name");

            _TxtLastNameEntry = new Entry { Placeholder = "Last Name", VerticalOptions = LayoutOptions.Center };
            _TxtLastNameEntry.SetBinding(Entry.TextProperty, "aStudent.LastName");

            _TxtNumberEntry = new Entry { Placeholder = "Student Number", VerticalOptions = LayoutOptions.Center };
            _TxtNumberEntry.SetBinding(Entry.TextProperty, "aStudent.StudentNumber");

            #region PlusMinus labels
            var plusLabel = new Label {
                VerticalOptions = LayoutOptions.Start,
                WidthRequest = 50,
                HeightRequest = 50,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center
            };
            plusLabel.SetBinding(Label.TextProperty, "PlusCount");

            var plusSvg = new SvgImage {
                SvgPath = "TheGrader.Resources.Plus-Blur.svg",
                SvgAssembly = this.GetType().GetTypeInfo().Assembly,
                WidthRequest = 50,
                HeightRequest = 50,
                HorizontalOptions = LayoutOptions.Start
                    
            };

            var minusLabel = new Label {
                VerticalOptions = LayoutOptions.End,
                WidthRequest = 50,
                HeightRequest = 50,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center
            };
            minusLabel.SetBinding(Label.TextProperty, "MinusCount");



            var minusSvg = new SvgImage {
                SvgPath = "TheGrader.Resources.Minus-Blur.svg",
                SvgAssembly = this.GetType().GetTypeInfo().Assembly,
                WidthRequest = 50,
                HeightRequest = 50,
                HorizontalOptions =  LayoutOptions.End
            };

            var PlusMinusView = new RelativeLayout {
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
                WidthRequest = 50,
                HeightRequest = 50,
                
            };
            PlusMinusView.Children.Add(plusSvg,
                                    Constraint.Constant(0), Constraint.Constant(0),
                                    Constraint.RelativeToParent((parent) => { return parent.Width; }),
                                    Constraint.RelativeToParent((parent) => { return parent.Height; })
                                      );
            PlusMinusView.Children.Add(plusLabel,
                                    Constraint.Constant(0), Constraint.Constant(0),
                                    Constraint.RelativeToParent((parent) => { return parent.Width; }),
                                    Constraint.RelativeToParent((parent) => { return parent.Height; }));

            PlusMinusView.Children.Add(minusSvg,
                                    Constraint.Constant(75), Constraint.Constant(0),
                                    Constraint.RelativeToParent((parent) => { return parent.Width; }),
                                    Constraint.RelativeToParent((parent) => { return parent.Height; })
                                      );
            PlusMinusView.Children.Add(minusLabel,
                                    Constraint.Constant(75), Constraint.Constant(0),
                                    Constraint.RelativeToParent((parent) => { return parent.Width; }),
                                    Constraint.RelativeToParent((parent) => { return parent.Height; }));


            #endregion PlusMinus labels

            _DoneButton = new Button { Text = "Done", HorizontalOptions = LayoutOptions.Center };
            _DoneButton.SetBinding(Button.CommandProperty, "UpdateStudent");

            BindingContext = ViewModel;
            Content = new StackLayout {
                VerticalOptions = LayoutOptions.Start,
                Padding = new Thickness(20, 20),
                Children = {
                   _StudentImage,_TakePicture, _TxtNameEntry,_TxtLastNameEntry,_TxtNumberEntry, PlusMinusView, _DoneButton,

                }
            };
        }
		#endregion

		#region private methods
		#endregion
	}
}
