﻿using System;
using Xamarin.Forms;

namespace TheGrader
{
	public class NewStudentPage : ViewPage<NewStudentViewModel>
	{
		#region private fields
		private Entry _TxtNameEntry;
		private Entry _TxtLastNameEntry;
		private Entry _TxtNumberEntry;
		private Button _DoneButton;
		#endregion

		#region override
		protected override void OnAppearing()
		{
			base.OnAppearing();
		}
		#endregion

		#region Ctor
        public NewStudentPage( int pClassID )
		{
            Title = "Add New Student";
            ViewModel.Navigation = this.Navigation;
            ViewModel.aClassID = pClassID;
            ViewModel.aStudent = new TheGraderCore.Student();

            //Fields
            _TxtNameEntry = new Entry { Placeholder = "First Name", VerticalOptions = LayoutOptions.Center };
            _TxtNameEntry.SetBinding(Entry.TextProperty, "aStudent.Name");

            _TxtLastNameEntry = new Entry { Placeholder = "Last Name", VerticalOptions = LayoutOptions.Center };
            _TxtLastNameEntry.SetBinding(Entry.TextProperty, "aStudent.LastName");

            _TxtNumberEntry = new Entry { Placeholder = "Student Number", VerticalOptions = LayoutOptions.Center };
            _TxtNumberEntry.SetBinding(Entry.TextProperty, "aStudent.StudentNumber");

            _DoneButton = new Button { Text = "Done", HorizontalOptions = LayoutOptions.Center };
            _DoneButton.SetBinding(Button.CommandProperty, "SaveNewStudentToClassCommand");

            BindingContext = ViewModel;
            Content = new StackLayout {
                VerticalOptions = LayoutOptions.Start,
                Padding = new Thickness(20, 20),
                Children = {
                    _TxtNameEntry,_TxtLastNameEntry,_TxtNumberEntry, _DoneButton
                }
            };
        }

		#endregion

		#region private methods
		#endregion
	}
}
