﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using TheGraderCore;
using TheGrader;
using Xamarin.Forms.Internals;
using System.Reflection;

namespace TheGrader
{
	public class ClassesListPage : ViewPage<ClassesListPageViewModel>
	{
        #region private members
		// TODO silinecek gibi: private ObservableCollection<Class> classesList = new ObservableCollection<Class>();
        private ListView _ClassesListView;
        #endregion

		#region Override
		protected override void OnAppearing()
		{
			base.OnAppearing();
			_ClassesListView.ItemsSource = ViewModel.GetClassModelsList();
		}
        #endregion

		void listSelection( object sender, SelectedItemChangedEventArgs e )
		{
			ViewModel.StudentListCommand.Execute( e.SelectedItem );
		}

       
		public ClassesListPage()
		{
			ViewModel.Navigation = this.Navigation;
			Title = "The Grader";

			_ClassesListView = new ListView { HorizontalOptions = LayoutOptions.CenterAndExpand };
			_ClassesListView.ItemTemplate = new DataTemplate( () => { return new ClassesListViewCell( ViewModel ); } );
			_ClassesListView.RowHeight = ClassesListViewCell.RowHeight;
			_ClassesListView.ItemSelected += listSelection;

			ToolbarItems.Add( new ToolbarItem( "Add", "", async () => { await Navigation.PushAsync( new AddClassPage() ); } ) );

			MessagingCenter.Subscribe<ClassesListPageViewModel>( this, "UpdateClassList", 
                                                         ( obj ) => { _ClassesListView.ItemsSource = ViewModel.Classes; } );


            var tlbhomepage = new MenuToolBarItem(ViewModel.ToolBarHomeCommand, "TheGrader.Resources.Home.svg", "Ana Sayfa","Ana Sayfa");
            var tlbWeekly = new MenuToolBarItem(ViewModel.WeeklyScheduleCommand, "TheGrader.Resources.Calendar.svg", "Haftalık Program",null);
            var tlbImport = new MenuToolBarItem(ViewModel.ImportClassPageCommand, "TheGrader.Resources.import3-01.svg", "İthal", "ithal et");
             
            var bottomtoolbar = new MenuToolBar(new List<MenuToolBarItem>() { tlbhomepage, tlbWeekly,tlbImport });

        
            Content = new StackLayout
			{
                Padding = new Thickness(10, 10, 10, 10),
                
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Children = { _ClassesListView,bottomtoolbar }
			};
		}
	}
}

public class ClassesListViewCell : GestureViewCell
{
	public const int RowHeight = 40;

	public ClassesListViewCell( ClassesListPageViewModel pViewModel )
	{

		/* .......................................................................
		     _____________________________________________________
		    |           ||---------------------------------------||
		    |           ||                                       ||
		    |   Grade   ||          ClassName                    ||
		    |   Branch  ||_______________________________________||
		    |           ||                                       ||
		    |           ||           SchoolName                  ||
		    |___________||---------------------------------------||
		.......................................................................... */

		BindingContext = pViewModel;

		#region Left Layout Definition
		// Prepare first stack
		var classNameLabel = new Label
		{
			FontSize = Device.GetNamedSize( NamedSize.Medium, typeof( Label ) ),
			FontAttributes = FontAttributes.Bold,
			VerticalOptions = LayoutOptions.FillAndExpand,
			HeightRequest = 20,
			BackgroundColor = Color.Yellow,
		};
		classNameLabel.SetBinding( Label.TextProperty, "ClassName" );

		var schoolLabel = new Label
		{
			FontSize = Device.GetNamedSize( NamedSize.Micro, typeof( Label ) ),
			FontAttributes = FontAttributes.Italic,
			VerticalOptions = LayoutOptions.FillAndExpand,
			HeightRequest = 10, 
			BackgroundColor = Color.Green,
		};
		schoolLabel.SetBinding( Label.TextProperty, "SchoolName" );

		var aStackLayout = new StackLayout
		{
			VerticalOptions = LayoutOptions.FillAndExpand,
			HorizontalOptions = LayoutOptions.FillAndExpand,
			Spacing = 2,
			Padding = 1,
			Children = { classNameLabel, schoolLabel },
			BackgroundColor = Color.Fuchsia,
		};
		#endregion

		var gradeBranchLabel = new Label
		{
			WidthRequest = 45,
			BackgroundColor = Color.Aqua,
			TextColor = Color.Gray,
			VerticalTextAlignment = TextAlignment.Center,
			HorizontalTextAlignment = TextAlignment.Start,
		};
		gradeBranchLabel.SetBinding( Label.TextProperty, "GradeBranch" );

        #region Context Menus
        var contDelete = new MenuItem { Text = "Delete", IsDestructive=true };
		contDelete.SetBinding( MenuItem.CommandParameterProperty, new Binding( "." ) );
		contDelete.Command = pViewModel.DeleteClassCommand;

		var contEdit = new MenuItem { Text = "Edit",  };
		contEdit.SetBinding( MenuItem.CommandParameterProperty, new Binding( "." ) );
		contEdit.Command = pViewModel.EditClassCommand;

		ContextActions.Add( contDelete );
		ContextActions.Add( contEdit );
		#endregion

		View = new StackLayout
		{
			Orientation=StackOrientation.Horizontal,
			HorizontalOptions=LayoutOptions.FillAndExpand,
			VerticalOptions=LayoutOptions.FillAndExpand,
			Spacing = 2,
			Padding = 3,
			Children = { gradeBranchLabel, aStackLayout },
		};
	}

}