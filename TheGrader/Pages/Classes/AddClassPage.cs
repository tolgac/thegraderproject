﻿using System;

using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace TheGrader
{
	public class AddClassPage : ViewPage<AddClassViewModel>
	{
		#region override
		protected override void OnAppearing()
		{
			base.OnAppearing();
		}
		#endregion

		public AddClassPage()
		{
			ViewModel.Navigation = this.Navigation;
			ViewModel.NewClassSchoolName = "";
			ViewModel.NewClassName = "";
			ViewModel.NewClassGrade = "";
			ViewModel.NewClassBranch = "";

			var txtSchoolName = new Entry { Placeholder = "School Name", VerticalOptions = LayoutOptions.Center };
			txtSchoolName.SetBinding( Entry.TextProperty, "NewClassSchoolName" );

			var txtLessonName = new Entry { Placeholder = "Lesson Name", VerticalOptions = LayoutOptions.Center };
			txtLessonName.SetBinding( Entry.TextProperty, "NewClassName" );

			var txtGradeDescription = new Entry { Placeholder = "Grade", VerticalOptions = LayoutOptions.Center };
			txtGradeDescription.SetBinding( Entry.TextProperty, "NewClassGrade" );

			var txtBranchDescription = new Entry { Placeholder = "Branch", VerticalOptions = LayoutOptions.Center };
			txtBranchDescription.SetBinding( Entry.TextProperty, "NewClassBranch" );

			var btnSave = new Button { Text = "Done", HorizontalOptions = LayoutOptions.Center, };
			btnSave.SetBinding( Button.CommandProperty, "SaveNewClassCommand" );

			//ViewModel.Clear();
			BindingContext = ViewModel;
			Content = new StackLayout
			{
				VerticalOptions = LayoutOptions.Start,
				Padding = new Thickness(20, 20),
				Children = {
					txtSchoolName, txtLessonName, txtGradeDescription, txtBranchDescription, btnSave,
				},
			};
		}

	} // class AddClassPage
} // namespace TheGrader

