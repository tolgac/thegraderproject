﻿using System;
using Xamarin.Forms;
using TheGrader;
using SVG.Forms.Plugin.Abstractions;
using System.Reflection;
using Autofac.Util;
using System.Windows.Input;

namespace TheGrader
{
    public class HomePage2 : ViewPage<HomePageViewModel>
    {
        #region Private Fields
        private Image _LessonsImage;
        private int _Height;
        private int _Width;
        #endregion

        #region Override
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
        #endregion

        public HomePage2()
        {
            _Height = 100;
            _Width = 100;
            WriteResources();

            ViewModel.Navigation = this.Navigation;
            Title = "Öğretmen Ajandası";

            var firstLine = new HorizontalTwoButtonLayout(100, 100);
            firstLine.SetFirstSvgButton( "TheGrader.Resources.Plus.svg", "Ders Listesi", ViewModel.LessonsTapCommand, "Classes" );
            firstLine.SetSecondSvgButton("TheGrader.Resources.Calendar.svg", "Ders Planı", ViewModel.LessonsTapCommand, "Schedule");

            var secondLine = new HorizontalTwoButtonLayout(100, 100);
            secondLine.SetFirstSvgButton("TheGrader.Resources.svg.149-cog.svg", "Ayarlar", ViewModel.LessonsTapCommand, "Settings");
            secondLine.SetSecondSvgButton("TheGrader.Resources.svg.270-cancel-circle.svg", "", ViewModel.LessonsTapCommand, "NA");

            //Content = new StackLayout {
            //    Orientation = StackOrientation.Vertical,
            //    HorizontalOptions = LayoutOptions.CenterAndExpand,
            //    VerticalOptions = LayoutOptions.Start,
            //    Children = { firstLine.GetView, secondLine.GetView }, // {  lessonsImageButton, scheduleImageButton },
            //    Spacing = 20,
            //    Padding = 10,
            //};

            var grid = new Grid {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                ColumnSpacing = 5,
                RowSpacing = 5,
                // BackgroundColor = Color.Maroon,
            };
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            // grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            grid.Children.Add( LoadButton("TheGrader.Resources.Plus.svg", ViewModel.LessonsTapCommand, "Classes"), 0, 0);
            grid.Children.Add( LoadButton("TheGrader.Resources.Calendar.svg", ViewModel.LessonsTapCommand, ""), 0, 1 );
            grid.Children.Add( new Button { Text = "Buton-1", BackgroundColor = Color.Fuchsia }, 1, 0 );
            grid.Children.Add( LoadImage("TheGrader.Resources.svg.270-cancel-circle.svg"), 1, 1);
            grid.Children.Add(new Button { Text = "Buton-0-2", BackgroundColor = Color.Blue }, 0, 2);
            grid.Children.Add(new Button { Text = "Buton-1-2", BackgroundColor = Color.Blue }, 1, 2);
            // grid.Children.Add(new Button { Text = "Buton-2-2", BackgroundColor = Color.Blue }, 2, 2);
            grid.Children.Add(new Button { Text = "Buton-0-2", BackgroundColor = Color.Blue }, 0, 3);
            grid.Children.Add(new Button { Text = "Buton-1-2", BackgroundColor = Color.Blue }, 1, 3);
            // grid.Children.Add(new Button { Text = "Buton-2-2", BackgroundColor = Color.Blue }, 2, 3);


            System.Diagnostics.Debug.WriteLine( "Row0:" + grid.RowDefinitions[0].Height.Value );
            System.Diagnostics.Debug.WriteLine( "Column0:" + grid.ColumnDefinitions[0].Width.Value );

            Content = grid;
        }

        #region private methods
        private View LoadImage(string embeddedResourceName)
        {
            return new SvgImage {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                SvgPath = embeddedResourceName,
                SvgAssembly = this.GetType().GetTypeInfo().Assembly,
                HeightRequest = 100,
                WidthRequest = 100,
                // BackgroundColor = Color.Black
            };
        }

        private View LoadButton( string embeddedResourceName, ICommand buttonCommand, object cmdParameter )
        {
            var svgButton = new SvgImageButton() {
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
                SvgPath = embeddedResourceName,
                SvgAssembly = this.GetType().GetTypeInfo().Assembly,
                HeightRequest = _Height,
                WidthRequest = _Width,
                Command = buttonCommand,
                CommandParameter = cmdParameter,
                BackgroundColor = Color.Aqua,
            };

            return new StackLayout {
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Children = { svgButton },
                Spacing = 1,
                Padding = 1,
                BackgroundColor = Color.Fuchsia,
            };
        }
        #endregion

        void WriteResources()
        {
            System.Diagnostics.Debug.WriteLine(">>> Embedded resources: Application ");
            var assembly = this.GetType().GetTypeInfo().Assembly;
            System.Diagnostics.Debug.WriteLine(">>> Embedded resources: assembly.fullname: " + assembly.FullName);
            System.Diagnostics.Debug.WriteLine(">>> Embedded resources: Application " + assembly.GetLoadableTypes());
            foreach (var res in assembly.GetManifestResourceNames()) {
                System.Diagnostics.Debug.WriteLine(">>> " + res);
            }
            //
            System.Diagnostics.Debug.WriteLine("\n\n>>> Embedded resources. HomePage");
            assembly = this.GetType().GetTypeInfo().Assembly;
            foreach (var res in assembly.GetManifestResourceNames()) {
                System.Diagnostics.Debug.WriteLine(">>> " + res);
            }
            ////
            //System.Diagnostics.Debug.WriteLine("\n\n>>> Embedded resources. TheGraderCore.Class");
            //assembly = typeof( TheGraderCore.Class ).GetType().GetTypeInfo().Assembly;
            //foreach( var res in assembly.GetManifestResourceNames() )
            //{
            //  System.Diagnostics.Debug.WriteLine( ">>> " + res );
            //}
            ////
            //System.Diagnostics.Debug.WriteLine("\n\n>>> Embedded resources. DBCore.ClassService");
            //assembly = typeof( DBCore.ClassService ).GetType().GetTypeInfo().Assembly;
            //foreach( var res in assembly.GetManifestResourceNames() )
            //{
            //  System.Diagnostics.Debug.WriteLine( ">>> " + res );
            //}
        }
    }

    public class HorizontalTwoButtonLayout2 : StackLayout
    {
        #region Private Fields
        private StackLayout _aStackLayout;
        private int _Height;
        private int _Width;
        private View _First;
        private View _Second;
        #endregion

        public View GetView {
            get {
                if( null == _aStackLayout )
                {
                    _aStackLayout = new StackLayout {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        VerticalOptions = LayoutOptions.CenterAndExpand,
                        Spacing = 30,
                        Padding = 15,
                        // BackgroundColor = Color.Black,
                    };

                    if(null != _First) _aStackLayout.Children.Add( _First );
                    if(null != _Second) _aStackLayout.Children.Add( _Second );
                }
                return _aStackLayout;
            }
        }

        #region Constructor
        public HorizontalTwoButtonLayout2( int height, int width )
        {
            this._Height = height;
            this._Width = width;

        }
        #endregion

        public void SetFirstSvgButton( string embeddedName, string label, ICommand buttonCommand, object cmdParameter )
        {
            SetSvgButton(out _First, embeddedName, label, buttonCommand, cmdParameter);
        }

        public void SetSecondSvgButton(string embeddedName, string label, ICommand buttonCommand, object cmdParameter)
        {
            SetSvgButton(out _Second, embeddedName, label, buttonCommand, cmdParameter);
        }

        private void SetSvgButton(out View param, string embeddedName, string label, ICommand buttonCommand, object cmdParameter)
        {
            var svgImg = new SvgImageButton() {
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
                SvgPath = embeddedName,
                SvgAssembly = this.GetType().GetTypeInfo().Assembly,
                HeightRequest = _Height,
                WidthRequest = _Width,
                Command = buttonCommand,
                CommandParameter = cmdParameter,
                BackgroundColor = Color.Aqua,
            };

            var aLabel = new Label() {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.Start,
                Text = label,
                HeightRequest = 20,
                WidthRequest = _Width,
                BackgroundColor = Color.Fuchsia,
            };

            param = new StackLayout() {
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Spacing = 1,
                Padding = 1,
                Children = { svgImg, aLabel }
            };
        }
    }
}
