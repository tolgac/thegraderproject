﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Input;
using SVG.Forms.Plugin.Abstractions;
using Xamarin.Forms;
namespace TheGrader
{
    public class MenuToolBar : StackLayout
    { 
        public MenuToolBar(List<MenuToolBarItem> menuitems)
        {
            
            this.Orientation = StackOrientation.Horizontal;
            HorizontalOptions = LayoutOptions.CenterAndExpand;

            foreach (var menu in menuitems) {
                Children.Add(menu);
            }
        }
    }
    public class MenuToolBarItem:ContentView
    {
        #region Ctor
         
        public MenuToolBarItem(ICommand command, string svgpath, string caption, object commandparameter)
        {
            _svgpath = svgpath;
            _caption = caption;
            _command = command;
            _commandparamter = commandparameter;
            VerticalOptions = LayoutOptions.Center;
            Padding = new Thickness(10, 0, 10, 0);

            //Image
            _image = new SvgImage {
                SvgPath = svgpath,
                SvgAssembly = this.GetType().GetTypeInfo().Assembly,
                HeightRequest = 32,
                WidthRequest = 32,
                 HorizontalOptions= LayoutOptions.Center
            };

            //Label
            if (string.IsNullOrEmpty(caption)) {
                
            }
            _label = new Label { Text = caption, HorizontalOptions = LayoutOptions.Center, FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)) };

            var tapgesture = new TapGestureRecognizer();
            tapgesture.Command = _command;
            tapgesture.CommandParameter = _commandparamter;

            this.GestureRecognizers.Add(tapgesture);

            this.Content = new StackLayout {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Children = { _image, _label }
            };
        }
        #endregion

        #region Private Fields
        private SvgImage _image;
        private Label _label;

        private readonly string _svgpath;
        private readonly string _caption;
        private ICommand _command;
        private object _commandparamter;
        #endregion



    }

}
