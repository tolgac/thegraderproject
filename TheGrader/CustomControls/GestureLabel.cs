﻿using System;
using Xamarin.Forms;
using System.Windows.Input;

namespace TheGrader
{
	public class GestureLabel : AbsoluteLayout
	{
		// member variables
		public Label aLabel;

		// member properties
		public static readonly BindableProperty CommandParameterProperty =
  			BindableProperty.Create("CommandParameter", typeof(object), typeof(GestureLabel), null);


		public static readonly BindableProperty TextProperty =
  			BindableProperty.Create("Text", typeof(string), typeof(GestureLabel), "");

		public static readonly BindableProperty LongPressCommandProperty =
			BindableProperty.Create("LongPressCommand", typeof(ICommand), typeof(GestureLabel), null);

		public static readonly BindableProperty SingleTappedCommandProperty =
			BindableProperty.Create("SingleTappedCommand", typeof(ICommand), typeof(GestureLabel), null);


		public object CommandParameter
		{
			get { return (object)GetValue(CommandParameterProperty);}
			set {SetValue(CommandParameterProperty,value);}
		}
		public string Text
		{
			get { return (string)GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}

		public ICommand LongPressCommand 
		{ 
			get { return (ICommand)GetValue(LongPressCommandProperty);}
			set {SetValue(LongPressCommandProperty,value);}
		}
		public ICommand SingleTappedCommand 
		{
			get { return (ICommand)GetValue(SingleTappedCommandProperty);} 
			set {SetValue(SingleTappedCommandProperty,value);}
		}

		protected override void OnPropertyChanged(string propertyName = null)
		{
			
			base.OnPropertyChanged(propertyName);
			if (propertyName=="Text")
			{
				if (aLabel !=null)
				{
					aLabel.Text = Text;
				}
			}

			if (propertyName=="CommandParameter")
			{
				 
			}
		}


		public GestureLabel()
		{
			var ges = new GestureView();
			ges.HorizontalOptions = LayoutOptions.FillAndExpand;
			ges.VerticalOptions = LayoutOptions.FillAndExpand;
			ges.LongPressed += (sender, e) =>
			{
				if (LongPressCommand != null && LongPressCommand.CanExecute(null))
				{
					//LongPressCommand.Execute(Labelx);
					LongPressCommand.Execute(this);
					
				}
			};
			ges.SingleTapped += (sender, e) => {
				if (SingleTappedCommand!=null && SingleTappedCommand.CanExecute(null))
				{
					//SingleTappedCommand.Execute(Labelx);
					SingleTappedCommand.Execute(this);
					
				}
			};
			 aLabel = new Label
			{
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			aLabel.InputTransparent = true;
			aLabel.SizeChanged += (sender, e) =>
			{
				ges.WidthRequest = this.Width;
				ges.HeightRequest = this.Height;

			};
			this.Children.Add(aLabel, new Point(0, 0));
			this.Children.Add(ges, new Point(0, 0));

			aLabel.Text = Text ?? aLabel.Text;
			this.BindingContextChanged += ( sender, e ) => { aLabel.Text = Text ?? aLabel.Text; };
		}

	} // class Gesture Label

	public class GestureView : View
	{
		public GestureView() : base()
		{
		}

		public event EventHandler LongPressed;
		public event EventHandler SingleTapped;
		public event EventHandler DoubleTapped;

		public void OnLongPressed(object sender, EventArgs e)
		{
			if (LongPressed != null)
			{
				LongPressed(this, e);
			}
		}

		public void onSingleTapped(object sender, EventArgs e)
		{
			if (SingleTapped!=null)
			{
				SingleTapped(this, e);
			}
		}
		public void onDoubleTapped(object sender, EventArgs e)
		{ 
			if (DoubleTapped!=null)
			{
				DoubleTapped(this, e);
			}
		}
	} // GestureView
}

