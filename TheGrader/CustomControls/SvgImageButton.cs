﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using SVG.Forms.Plugin.Abstractions;
using Xamarin.Forms;

namespace TheGrader
{
    public class SvgImageButton : SvgImage
    {

        public static readonly BindableProperty CommandProperty = 
            // BindableProperty.Create<SvgImageButton, ICommand>(p => p.Command, null);
            BindableProperty.Create("Command", typeof(ICommand), typeof(SvgImageButton), null);

        public ICommand Command {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }


        public static readonly BindableProperty CommandParameterProperty = // BindableProperty.Create<SvgImageButton, object>(p => p.CommandParameter, null);
        BindableProperty.Create("CommandParameter", typeof(object), typeof(SvgImageButton), null);
        public object CommandParameter {
            get { return (object)GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        private ICommand TransitionCommand {
            get {
                return new Command(async () => {
                    this.AnchorX = 0.48;
                    this.AnchorY = 0.48;
                    await this.ScaleTo(0.8, 50, Easing.Linear);
                    //await Task.Delay(100);
                    await this.ScaleTo(1, 50, Easing.Linear);
                    if (Command != null) {
                        Command.Execute(CommandParameter);
                    }
                });
            }
        }

        public SvgImageButton()
        {
            Initialize();
        }


        public void Initialize()
        {
            GestureRecognizers.Add(new TapGestureRecognizer() {
                Command = TransitionCommand
            });
        }

    }
}
