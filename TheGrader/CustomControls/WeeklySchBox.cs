﻿using System;
using Xamarin.Forms;

namespace TheGrader
{
	public class WeeklySchClassItem: WeeklySchBox
	{
		#region Ctor
		public WeeklySchClassItem(string caption):base(caption)
		{
			this.Caption = caption;
			 
		}
		#endregion

		#region public fields
		public bool IsAssigned { get; set; }
        public int ID { get; set; }

        public bool IsOnDB { get; set; }
        public bool IsRemoved { get; set; }
		#endregion

		#region private fields

		#endregion
	}
	public class WeeklySchBox:ContentView
	{
		  #region Ctor
        public WeeklySchBox(string caption)
        {
            _normalColor = Color.Accent;
            _highlightColor = Color.FromHex("f16206");
            this.BackgroundColor = Color.Maroon;
            this.HeightRequest = 40;
            caption = _caption;
            ClassID = -1;
            this.Padding = new Thickness(0);

            //Rounded corner container
            _label = new Label {
                Text = caption,
                FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)),
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
                //FontSize = 10
            };
            //_label.Labelx.FontSize = 10;

            this.Content = new Frame {

                HasShadow = false,
                OutlineColor = _normalColor,
                Padding = new Thickness(3, 3, 3, 3),
                Content = new StackLayout {
                    HorizontalOptions = LayoutOptions.Center,
                    VerticalOptions = LayoutOptions.Center,
                    Spacing = 0,
                    Children = {
                        _label
                    }
                }
            };
            this.BackgroundColor = Color.Transparent;

        }
        #endregion

        #region private fields

        private Label _label;
        private string _caption;
        private Color _normalColor;
        private Color _highlightColor;

        #endregion

        #region Public Fields
        public Rectangle globalRectangle { get; set; }

        public double XX { get; set; }
        public double YY { get; set; }

        public int Day { get; set; }
        public int Session { get; set; }
        public WeeklySchBox Lesson { get; set; }
        public string Caption {
            get { return _caption; }
            set {
                _caption = value;
                _label.Text = value;
            }
        }
        public int ClassID { get; set; }
        #endregion

        #region Public Methods


        public bool IsInsight(Point point)
        {

            globalRectangle = new Rectangle(this.X, this.Y, this.Width, this.Height);
            var result = globalRectangle.Contains(point);
            return result;
        }
        public void HighlightBox(bool highlight)
        {
            if (highlight) {
                (this.Content as Frame).OutlineColor = _highlightColor;
            } else {
                (this.Content as Frame).OutlineColor = _normalColor;
            }

        }
        #endregion
	}


}
