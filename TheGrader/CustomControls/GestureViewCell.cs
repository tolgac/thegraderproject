﻿using System;
using Xamarin.Forms;

namespace TheGrader
{
	public class GestureViewCell : ViewCell
	{
		public GestureViewCell():base()
		{
			this.View = new GestureView();

			this.View.BackgroundColor = Color.Red;
		}

	} // class GestureViewCell
} // namespace TheGrader

