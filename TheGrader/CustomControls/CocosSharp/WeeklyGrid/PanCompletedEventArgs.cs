﻿using System;
using CocosSharp;
namespace TheGrader
{
    public class PanCompletedEventArgs:EventArgs
    {
        #region Ctor
        public PanCompletedEventArgs(CCPoint endPoint)
        {
            _endPoint = endPoint;
        }
        #endregion

        #region Private fields
        private CCPoint _endPoint;
        #endregion

        #region Properties
        public CCPoint EndPoint { 
            get {
                return _endPoint;
            }
        }
        #endregion
    }
}
