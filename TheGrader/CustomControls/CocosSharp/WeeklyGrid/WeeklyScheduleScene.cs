﻿using System;
using System.Windows.Input;
using CocosSharp;

namespace TheGrader
{
    public class WeeklyScheduleScene:CCScene
    {
        #region Ctor
        public WeeklyScheduleScene(CCGameView gameview, ICommand OpenClassListCommand):base(gameview)
        {
            BaseLayer = new WeeklyGridLayer(OpenClassListCommand);
            this.AddLayer(BaseLayer);
        }
        #endregion

        #region Public Fields
        public WeeklyGridLayer BaseLayer;
        #endregion

        
    }


}
