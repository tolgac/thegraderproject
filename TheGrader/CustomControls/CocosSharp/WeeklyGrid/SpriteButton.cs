﻿using System;
using System.Windows.Input;
using CocosSharp;
using Xamarin.Forms;
namespace TheGrader
{
     
    public class SpriteButton:CCNode
    {
      
        #region Ctor
        public SpriteButton(string pngimage,CCRect rect,int padding=0):base()
        {
            
            _gridrect = rect;
            _sprite = new CCSprite(pngimage);
            _sprite.ContentSize = rect.Size-padding;
            this.ContentSize = rect.Size;
            this.Position = rect.Center;
            
            touchListener = new CCEventListenerTouchAllAtOnce();
            touchListener.OnTouchesBegan += TouchListener_OnTouchesBegan;
            touchListener.OnTouchesEnded += TouchListener_OnTouchesEnded;

            this.AddEventListener(touchListener);
            AddChild(_sprite);

        }
        #endregion
         
        #region Private Fields
        CCSprite _sprite;
        CCEventListenerTouchAllAtOnce touchListener;
        CCRect _gridrect;
        bool _cantap;
        #endregion

        #region PublicFields
        public ICommand TapCommand;
        public object CommandParameter;
        #endregion
        #region Event Methods
        void TouchListener_OnTouchesEnded(System.Collections.Generic.List<CCTouch> arg1, CCEvent arg2)
        {
            if (_cantap) {
                //todo
                var action = new CCScaleTo(.1f, 1 , 1 );
                this.AddAction(action);
                if (TapCommand!=null && TapCommand.CanExecute(CommandParameter)) {
                    TapCommand.Execute(CommandParameter);
                }
            }
            _cantap = false;
        }

        void TouchListener_OnTouchesBegan(System.Collections.Generic.List<CCTouch> arg1, CCEvent arg2)
        { 
            if (this._gridrect.ContainsPoint(arg1[0].Location)) {
                
                var action1 = new CCScaleTo(.1f, this.ScaleX*0.7f,this.ScaleY*0.7f);
                this.AddAction(action1); 
                _cantap = true;
            }
        }

        #endregion
    }
}
