﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using CocosSharp;
using Xamarin.Forms;
namespace TheGrader
{
    public class SessionItem : DragButton
    { 
        #region Ctor
        public SessionItem(GridRect rect, string caption):base(rect,caption)
        {

        }
        #endregion
    }
    public class DragButton:CCDrawNode
    {
        bool HandleFunc()
        {
            touchlistener.OnTouchesMoved += Touchlistener_OnTouchesMoved;
            return true;
        }

        #region Overrides


        protected override void AddedToScene()
        {
            base.AddedToScene();

            touchlistener = new CCEventListenerTouchAllAtOnce();
            //touchlistener.OnTouchesMoved += Touchlistener_OnTouchesMoved;
            touchlistener.OnTouchesBegan += Touchlistener_OnTouchesBegan;
            touchlistener.OnTouchesEnded += Touchlistener_OnTouchesEnded;
            this.AddEventListener(touchlistener, this);

            _cclabel = new CCLabel(_strLabel, "San Fransisco", 28f, CCLabelFormat.SystemFont);
            AddChild(_cclabel);

            _cclabel.PositionX = GridRect.Rect.MidX;
            _cclabel.PositionY = GridRect.Rect.MidY;
            _cclabel.Color = CCColor3B.Black;

            //DrawRect(_kzrect.Rect, CCColor4B.White, 1.5f, CCColor4B.Orange);
            this.ZOrder = 20;

        }
        #endregion

        #region Ctor
        public DragButton(GridRect rect, string caption)
        {
            GridRect = rect.Clone();
          
            _strLabel = caption;
            guid = new Guid();

        }

        #endregion

        #region Properties

        public string Label 
        {
            get { return _strLabel; }
            set 
            {
                _strLabel = value;
                _cclabel.Text = _strLabel;
            }
        }
      
        public int Day;
        public int Session;
        public bool IsOnDB;
        public bool IsRemoved;
        public int ID;
        public int ClassID;
        public GridRect GridRect;

        public Guid guid;
      
        public ICommand TapCommand;
        public Object CommandParameter;
        #endregion

        #region Private fields
        CCEventListenerTouchAllAtOnce touchlistener;

        CCLabel _cclabel;
        bool _Canmove;
        bool _IsPan;
        string _strLabel;
        #endregion

        #region Events
        public event EventHandler<PanCompletedEventArgs> PanCompleted;
        public event EventHandler Tapped;
        #endregion

        #region Private Methods
        void OnPanCompleted(PanCompletedEventArgs args)
        {
            if (PanCompleted != null) {
                Device.BeginInvokeOnMainThread(() => {
                    PanCompleted(this, args);
                });
            }
        }

        void OnTapped(EventArgs args)
        {
            
            if (Tapped != null) {
                //UI thrad gerekiyor
                 Device.BeginInvokeOnMainThread(() => {
                Tapped(this, args);
                });
            }
        }

        void Touchlistener_OnTouchesBegan(List<CCTouch> arg1, CCEvent arg2)
        {
           
         
            CCAffineTransform localTransform = AffineWorldTransform;
            CCRect worldtransformedBounds = localTransform.Transform(new CCRect(this.GridRect.Rect.Origin.X, this.GridRect.Rect.Origin.Y, ContentSize.Width, ContentSize.Height));
            var tw = worldtransformedBounds;
            if (tw.ContainsPoint(arg1[0].Location)) {
                _Canmove = true;
                //androidte tap çok zor oluyor. bir süre beklemek gerekiyor.
                Device.StartTimer(new TimeSpan(1200), HandleFunc);
            }

        }
        void Touchlistener_OnTouchesEnded(List<CCTouch> arg1, CCEvent arg2)
        {
            if (_Canmove&&_IsPan) {
                
                OnPanCompleted(new PanCompletedEventArgs(arg1[0].Location));
                 
            }
            if (_Canmove && !_IsPan) {
                //if (TapCommand != null && TapCommand.CanExecute(CommandParameter)) {
                //    TapCommand.Execute(CommandParameter);
                //}
                OnTapped(new EventArgs());
            }
            _Canmove = false;
            this._IsPan = false;

            if (touchlistener.OnTouchesMoved!=null) {
                touchlistener.OnTouchesMoved -= Touchlistener_OnTouchesMoved;
                
            }
            
        }
        void Touchlistener_OnTouchesMoved(System.Collections.Generic.List<CCTouch> arg1, CCEvent arg2)
        {
            //Todo gesturelar daha doğru bir şekilde handle edilmeli. touccount vs.
            var e = arg1[0];
            if (Math.Abs(e.Delta.X)>2 | Math.Abs(e.Delta.Y)>2) {
                _IsPan = true;
                 
                if (_Canmove) {
                    //System.Diagnostics.Debug.WriteLine("----------------------------------");
                    //System.Diagnostics.Debug.WriteLine("Delta = " + e.Delta);
                    //System.Diagnostics.Debug.WriteLine("Location = " + e.Location);
                    //System.Diagnostics.Debug.WriteLine("Start Location = " + e.StartLocation);
                    //System.Diagnostics.Debug.WriteLine("----------------------------------");
                    //System.Diagnostics.Debug.WriteLine("");

                    //arg2.CurrentTarget.PositionX += e.Delta.X;
                    //arg2.CurrentTarget.PositionY += e.Delta.Y;
                    this.PositionX = (e.Location.X) - this.BoundingRect.Origin.X;
                    this.PositionY = (e.Location.Y) - this.BoundingRect.Origin.Y;

                }
            }

        }
        #endregion

    }

    public class GridRect
    {
        
        #region Ctor
        
        public GridRect(CCRect rect)
        {
            Rect = rect; 
        }
        #endregion

        #region Properties
        public CCRect Rect;
        public int ColumnIndex;
        public int RowIndex;
        public Action OnLanded;
        #endregion

        #region Public Methods
        public int GetDay(  int daystartcolumn)
        {
            return this.ColumnIndex - daystartcolumn;
            
        }
        public int GetSession(int sessionstartrow)
        {
            return this.RowIndex - sessionstartrow;
        }
        public GridRect Clone( )
        {
            var cloned = new GridRect(this.Rect);
            cloned.ColumnIndex = this.ColumnIndex;
            cloned.RowIndex = this.RowIndex;

            return cloned;
        }
        #endregion


    }
}
