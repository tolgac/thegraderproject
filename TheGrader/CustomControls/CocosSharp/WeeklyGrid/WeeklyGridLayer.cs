﻿using System.Linq;
using System.Collections.Generic;
using CocosSharp;
using System;
using System.Windows.Input;

namespace TheGrader
{
    public class WeeklyGridLayer: CCLayerColor
    {
        #region Ctor
        public WeeklyGridLayer(ICommand OpenClassListCommand) : base(CCColor4B.White)
        {
            _boxes = new List<GridRect>();
            _Sessions = new List<GridRect>();
            _OpenClassListCommand = OpenClassListCommand;

        }
        #endregion
            
        #region Overrides
        protected override void AddedToScene()
        {
            base.AddedToScene();
            SetupGrid(9);
            AddSesssions();
            AddDays();
            AddCircles();
            AssignedBoxes = new List<DragButton>();

        }
        #endregion

        #region Private Fields
        private List<GridRect> _boxes;
        private List<GridRect> _Sessions;
        private ICommand _OpenClassListCommand;
        #endregion

        #region Private Methods
        void SetupGrid(int columnCount)
        {
            var bounds = VisibleBoundsWorldspace;
            var columncount = columnCount;
            var h = bounds.Size.Height / bounds.Size.Width;
            var rowcount = (int)(columncount * h);
            var rowheight = bounds.Size.Height / rowcount;
            var colwidth = (bounds.Size.Width / columncount);

            ApplicationConsts.ColumnWidth = colwidth;
            ApplicationConsts.RowHeight = rowheight;
            for (int i = 0; i < columncount; i++) {
                var p1 = new CCPoint(bounds.MinX + (i * colwidth), bounds.MinY);
                //var p2 = new CCPoint(bounds.MinX + (i * colwidth), bounds.MaxY);
                //var col = new CCDrawNode();
                //col.DrawLine(p1, p2, CCColor4B.Red, CCLineCap.Square);
                //AddChild(col);
                for (int j = 0; j < rowcount; j++) {
                    var p3 = new CCPoint(bounds.MinX, bounds.MaxY - (j * rowheight));
                    //var p4 = new CCPoint(bounds.MaxX, bounds.MaxY - (j * rowheight));
                    //var row = new CCDrawNode();
                    //row.DrawLine(p3, p4, CCColor4B.Red, CCLineCap.Square);
                    //AddChild(row);
                    var rect = new GridRect(new CCRect(p1.X, p3.Y, colwidth, rowheight));
                    rect.ColumnIndex = i;
                    rect.RowIndex = j;
                    _boxes.Add(rect);
                    //var lbl = new CCLabel(i + ","+ j, "San Fransisco", 32f, CCLabelFormat.SystemFont);
                    //lbl.Position = new CCPoint(rect.Rect.Origin.X + colwidth / 2, rect.Rect.Origin.Y - rowheight / 2);
                    //lbl.Color = CCColor3B.Black;
                    //AddChild(lbl);

                }
            }
        }

        void AddSesssions()
        {

            var cw = ApplicationConsts.SessionColumnStartIndex;
            var rh = ApplicationConsts.SessionRowStartIndex;

            var rects = _boxes.Where(x => x.ColumnIndex == cw).Where(x => x.RowIndex > rh);

            int i = 1;
            foreach (var rect in rects) {
                var lbl = new CCLabel(i + "", "San Fransisco", 32f, CCLabelFormat.SystemFont);
                //lbl.Position = new CCPoint(insRect.Rect.Origin.X + WeeklyConsts.ColumnWidth / 2, insRect.Rect.Origin.Y - WeeklyConsts.RowHeight / 2);
                lbl.Position = new CCPoint(rect.Rect.Center.X, rect.Rect.Center.Y);

                lbl.Color = CCColor3B.Black;
                AddChild(lbl);
                i++;
            }

        }

        void AddDays()
        {

            var cw = ApplicationConsts.DayColumnStartIndex;
            var rh = ApplicationConsts.DayRowStartIndex;

            var rects = _boxes.Where(x => x.ColumnIndex > cw).Where(x => x.RowIndex == rh);

            int i = 1;
            foreach (var rect in rects) {
                var lbl = new CCLabel(StaticData.Days[i - 1], "San Fransisco", 32f, CCLabelFormat.SystemFont);
                //lbl.Position = new CCPoint(insRect.Rect.Origin.X + WeeklyConsts.ColumnWidth / 2, insRect.Rect.Origin.Y - WeeklyConsts.RowHeight / 2);
                lbl.Position = new CCPoint(rect.Rect.Center.X, rect.Rect.Center.Y);

                lbl.Color = CCColor3B.Black;
                AddChild(lbl);
                i++;
            }

        }

        void AddCircles()
        {
            var cw = ApplicationConsts.DayColumnStartIndex;
            var rh = ApplicationConsts.DayRowStartIndex;

            var rects = _boxes.Where(x => x.ColumnIndex > cw).Where(x => x.RowIndex > rh);
            rects = rects.Where(x => x.ColumnIndex <= 7);
            int i = 1;
            foreach (var rect in rects) {
                var circle = new CCDrawNode();
                //circle.DrawCircle(rect.Rect.Center, (rect.Rect.Size.Width / 2)-5, CCColor4B.Orange);
                circle.DrawRect(rect.Rect, CCColor4B.Transparent, 2f, CCColor4B.Orange);

                var lbl = new CCLabel(rect.ColumnIndex + " " + rect.RowIndex, "San Fransisco", 32f, CCLabelFormat.SystemFont);
                //lbl.Position = new CCPoint(insRect.Rect.Origin.X + WeeklyConsts.ColumnWidth / 2, insRect.Rect.Origin.Y - WeeklyConsts.RowHeight / 2);
                lbl.Position = new CCPoint(rect.Rect.Center.X, rect.Rect.Center.Y);
                lbl.Color = CCColor3B.Black;

                AddChild(circle);
                AddChild(lbl);
                _Sessions.Add(rect);
                i++;
            }
        }


        CCMoveTo TranslateAction(DragButton btn, GridRect toBox, float duration = .3f)
        { 
            var xsign = 1;
            var ysign = 1;
            if (btn.GridRect.Rect.MaxX > toBox.Rect.MinX) {
                xsign = -1;
            }
            if (btn.GridRect.Rect.MaxY > toBox.Rect.MaxY) {
                ysign = -1;
            }

            //Action. 
            var y = ysign * Math.Abs(toBox.Rect.MaxY - btn.GridRect.Rect.MaxY);
            var x = xsign * Math.Abs(toBox.Rect.MaxX - btn.GridRect.Rect.MaxX);
            var action = new CCMoveTo(duration, new CCPoint(x, y));

            return action;
        }
        void TranslateTo(DragButton btn, GridRect toBox,float duration=.3f, bool changeIndcies=true)
        {
            var action = TranslateAction(btn, toBox, duration);
            btn.AddAction(action);
            if (changeIndcies) {
                btn.GridRect.ColumnIndex = toBox.ColumnIndex;
                btn.GridRect.RowIndex = toBox.RowIndex;
            }

        }
        #endregion

        #region Public Feilds
        public List<DragButton> AssignedBoxes;
        #endregion

        #region Public Methods
        public DragButton AddDragButton(int column, int row, string caption,int classid)
        { 
            //var dragins = _boxes.Where(x => x.ColumnIndex == ApplicationConsts.DragButtonColIndex).Where(x => x.RowIndex == ApplicationConsts.DragButtomRowIndex).FirstOrDefault();
            var dragins = _boxes.Where(x => x.ColumnIndex == column).Where(x => x.RowIndex == row).FirstOrDefault();
            
            
            if (dragins != null) {
                var dragbtn = new DragButton(dragins, caption);
                dragbtn.ClassID = classid;
                this.Layer.AddChild(dragbtn);
                dragbtn.PanCompleted += Dragbutton_Pancompleted;

                dragbtn.DrawRect(dragins.Rect, CCColor4B.White, 1f, CCColor4B.Orange);

                return dragbtn;
            }
            return null;
        }

        public DragButton AddSessionItem(GridRect gridrect, string caption, bool isondb, int classid, int id)
        {
            var dragbtn = new DragButton(gridrect, caption);
            dragbtn.IsOnDB = isondb;
            dragbtn.ClassID = classid;
            dragbtn.ID = id;
            dragbtn.Day = gridrect.GetDay(ApplicationConsts.DayColumnStartIndex);
            dragbtn.Session = gridrect.GetSession(ApplicationConsts.SessionRowStartIndex);

            this.Layer.AddChild(dragbtn);
            dragbtn.PanCompleted += SessionItem_Pancompleted;
            dragbtn.Tapped +=  (sender, e) => { 
                if (_OpenClassListCommand.CanExecute(null)) {
                     _OpenClassListCommand.Execute(1);
                }
            };
            dragbtn.DrawRect(gridrect.Rect, CCColor4B.White, 1f, CCColor4B.Orange);
            AssignedBoxes.Add(dragbtn);

            return dragbtn;
        }

        public DragButton AddSessionItem(int day, int session, string caption, bool isondb, int classid, int id)
        {
            var col = ApplicationConsts.DayColumnStartIndex + day;
            var row = ApplicationConsts.SessionRowStartIndex + session;

            var dragins = _boxes.Where(x => x.ColumnIndex == col).Where(x => x.RowIndex == row).FirstOrDefault();
            if (dragins != null) { 
                var dragbtn = new DragButton(dragins, caption);
                dragbtn.IsOnDB = isondb;
                dragbtn.ClassID = classid;
                dragbtn.ID = id;
                dragbtn.Day = day;
                dragbtn.Session = session;

                this.Layer.AddChild(dragbtn);
                dragbtn.PanCompleted += SessionItem_Pancompleted;
                dragbtn.Tapped += (sender, e) => {
                    if (_OpenClassListCommand.CanExecute(null)) {
                        _OpenClassListCommand.Execute(1);
                    }
                };
                dragbtn.DrawRect(dragins.Rect, CCColor4B.White, 1f, CCColor4B.Orange);
                AssignedBoxes.Add(dragbtn);

                return dragbtn;

            }
            return null;
            
        }

 
        public SpriteButton AddSaveButton(int column, int row, string caption)
        {
            var dragins = _boxes.Where(x => x.ColumnIndex == 6).Where(x => x.RowIndex == 1).FirstOrDefault();

            var savebtn = new SpriteButton("flopy", dragins.Rect);

            AddChild(savebtn, 3);
            return savebtn;
        }

        public SpriteButton AddTrashButton(int column, int row)
        { 
        var dragins = _boxes.Where(x => x.ColumnIndex == 5).Where(x => x.RowIndex == 1).FirstOrDefault();

            var trashbtn = new SpriteButton("trash", dragins.Rect,20);

            AddChild(trashbtn, 3);
            return trashbtn;
        }
        #endregion


        #region Event Methods
        void Dragbutton_Pancompleted(object sender, PanCompletedEventArgs e)
        { 
            var btn = sender as DragButton;
            var boxes = _Sessions.Where(x => x.Rect.ContainsPoint(e.EndPoint));
            var box = _Sessions.FirstOrDefault(x => x.Rect.ContainsPoint(e.EndPoint));
            CCMoveTo move1 = null;
            CCMoveTo move2 = null;
            
            //Bulunan kutuya kaydır.
            if (box != null) {

                //altta ders varsa önceden sil 
                RemoveTheUnderSession(box.ColumnIndex, box.RowIndex,btn.guid);

                //konma efekti
                move1 = TranslateAction(btn, box,.3f); 
                 
               
                 
            }
          
            //Orjinal yerine gönder
            var r = _boxes.Where(x => x.ColumnIndex == 0).Where(x => x.RowIndex == 1).FirstOrDefault();
            move2 = TranslateAction(btn, r,0.0f);
            if (move1!=null) {
                btn.RunActions(new CCFiniteTimeAction[] { move1, move2 });
            }
            else {
                btn.RunActions(new CCFiniteTimeAction[] {   move2 });
            }

            //Ders ekle. Görsel algı yüzünden yukarı blokta değil burada yapıyorum.
            if (box!=null) {
                //Ders ekle
                AddSessionItem(box, btn.Label, btn.IsOnDB, btn.ClassID, btn.ID);
            }
        }

        void SessionItem_Pancompleted(object sender, PanCompletedEventArgs e)
        { 
            var btn = sender as DragButton;
            
            var box = _Sessions.FirstOrDefault(x => x.Rect.ContainsPoint(e.EndPoint));

            //Bulunan kutuya kaydır.
            if (box != null) {
                RemoveTheUnderSession(box.ColumnIndex,box.RowIndex,btn.guid);

                TranslateTo(btn, box);
                  
                btn.Day = box.GetDay(ApplicationConsts.DayColumnStartIndex);
                btn.Session = box.GetSession(ApplicationConsts.SessionRowStartIndex);
                 
            }
            //Başka yere
            else {
                MoveToTrah(btn); 
            }
        }

        void MoveToTrah(DragButton dragbutton)
        {
            // var trashbox = _boxes.Where(x => x.ColumnIndex == 4).Where(x => x.RowIndex == 1);

            //TranslateTo(dragbutton, trashbox.First());

            //AssignedBoxes.Remove(dragbutton);
            dragbutton.IsRemoved = true;
            dragbutton.RemoveAllChildren();
            dragbutton.Clear();

        }

        //alttaki dersi silmek gerekli.
        bool RemoveTheUnderSession(int colindex, int rowindex,Guid guid)
        {
            //Kendisini silmesin diye guid kontrolu gerekli.
            var prev = AssignedBoxes.Where(x => x.GridRect.ColumnIndex == colindex).Where(x => x.GridRect.RowIndex == rowindex).Where(x=>x.guid !=guid).FirstOrDefault();
            if (prev!=null ) {
                MoveToTrah(prev);
                return true;
            }
            return false;
        }

        #endregion
    }
}
