﻿using System;
using Xamarin.Forms;

namespace TheGrader
{
   public static class GridStyles
    {
        public static Color LightGrey = Color.FromHex("929292");
        public static Color DarkGrey = Color.FromHex("707070");

        public static Style HoursLabelStyle {
            get {
                return new Style(typeof(Label)) {
                    Setters = {
                        new Setter{Property = Label.FontSizeProperty,Value=10},
                        new Setter{Property = Label.TextProperty, Value =LightGrey },
                        new Setter{Property = Label.HorizontalTextAlignmentProperty, Value = TextAlignment.Start}
                    }
                };
            }
        }
        public static Style DaysLabelStyle {
            get {
                return new Style(typeof(Label)) {
                    Setters = {
                        new Setter { Property = Label.FontSizeProperty, Value = 12 },
                        new Setter { Property = Label.TextColorProperty, Value = LightGrey },
                        new Setter { Property = Label.VerticalOptionsProperty, Value = LayoutOptions.Center },
                        new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.Center }
                    }
                };
            }
        }
    }
}
